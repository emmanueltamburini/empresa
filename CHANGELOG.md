### Version 1.0.4
* Added support to Docker ECR #T9E-938

### Version 1.0.3
* API protected behind an authorization key #T9E-833

### Version 1.0.2
* NOT A RELEASE VERSION
* Improve the robustness of API fields ( firstname, lastname)

### Version 1.0.1
* Added the placeholder {datetime} for the signature top and bottom mention #T9E-800

### Version 1.0.0
* First draft on Java/Spring #T9E-597
  * Added app version endpoint
  * Added consume PDF endpoint
  * Added consume code endpoint
  * Added retrieve the PDF endpoint
* Integrate YouSign with the Electronic Signature service #T9E-633
* Added endpoint to get the signature status #T9E-586