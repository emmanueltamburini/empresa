package fr.testamento.es.service;

import static fr.testamento.es.utils.Messages.INTERNAL_SERVER_ERROR;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.testamento.es.exceptions.RestException;
import fr.testamento.es.utils.Utilities;
import fr.testamento.es.utils.YouSignErrorResponse;
import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@TestPropertySource(properties = {
		"yousign.api-key=1234567890",
		"yousign.base-url=https://electronic.signature/test"
})
public class ElectronicSignatureServiceTest extends TestCase {

	@TestConfiguration
    static class ElectronicSignatureServiceTestContextConfiguration {

        @Bean
        public ElectronicSignatureService electronicSignatureService() {
            return new ElectronicSignatureService();
        }
    }

	@Value("${yousign.api-key}")
    private String token;

	@Value("${yousign.base-url}")
    private String youSignUrl;

	@Autowired
	private ElectronicSignatureService esService;

	@MockBean
	public RestTemplate restTemplate;

	@MockBean
    private Utilities utilities;

	private String referenceCode;
	private byte[] file = "test".getBytes();
	private String route;
	private Object request = new Object();
	private String json;
	private final String value = "value";
	private YouSignErrorResponse youSignError = new YouSignErrorResponse(value, value, value);

	@Override
	@Before
    public void setUp() throws Exception {
        super.setUp();
        referenceCode = "{ \"detail\": \"error\"}";
        route = "/api/test";
        json = new ObjectMapper().writeValueAsString(youSignError);

    }

	@Test
    public void testGeneratePostRequest() {

        ResponseEntity<String> response = new ResponseEntity<String>(referenceCode, HttpStatus.OK);

        Mockito
        	.when(restTemplate.exchange(any(String.class), any(HttpMethod.class), any(HttpEntity.class), eq(String.class)))
        	.thenReturn(response);

        String test = esService.generatePostRequest(request, route);
    	Assert.assertEquals(response.getBody(), test);
    }

	@Test
    public void testGeneratePostIfApiReturnException() {

        Mockito.when(restTemplate.exchange(any(String.class), any(HttpMethod.class), any(HttpEntity.class), eq(String.class)))
        		.thenThrow(RestClientException.class);

        try {
        	esService.generatePostRequest(referenceCode, route);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
			Assert.assertEquals(INTERNAL_SERVER_ERROR, e.getMessage());
		}
    }

	@Test
    public void testGeneratePostIfApiReturnHttpClientErrorException() {

		HttpClientErrorException ex = new HttpClientErrorException(HttpStatus.BAD_REQUEST, "400", json.getBytes(), null);

        Mockito.when(utilities.stringToObject(json, YouSignErrorResponse.class))
        		.thenReturn(youSignError);

        Mockito.when(restTemplate.exchange(any(String.class), any(HttpMethod.class), any(HttpEntity.class), eq(String.class)))
		.thenThrow(ex);

        try {
        	esService.generatePostRequest(referenceCode, route);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
    }

	@Test
    public void testGeneratePutRequest() {

        ResponseEntity<String> response = new ResponseEntity<String>(referenceCode, HttpStatus.OK);

        Mockito
        	.when(restTemplate.exchange(any(String.class), any(HttpMethod.class), any(HttpEntity.class), eq(String.class)))
        	.thenReturn(response);

        String test = esService.generatePutRequest(request, route);
    	Assert.assertEquals(response.getBody(), test);
    }

	@Test
    public void testGeneratePutRequestIfApiReturnException() {

        Mockito.when(restTemplate.exchange(any(String.class), any(HttpMethod.class), any(HttpEntity.class), eq(String.class)))
        		.thenThrow(RestClientException.class);

        try {
        	esService.generatePutRequest(referenceCode, route);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
			Assert.assertEquals(INTERNAL_SERVER_ERROR, e.getMessage());
		}
    }

	@Test
    public void testGeneratePutRequestIfApiReturnHttpClientErrorException() {

		HttpClientErrorException ex = new HttpClientErrorException(HttpStatus.BAD_REQUEST, "400", json.getBytes(), null);

        Mockito.when(utilities.stringToObject(json, YouSignErrorResponse.class))
        		.thenReturn(youSignError);

        Mockito.when(restTemplate.exchange(any(String.class), any(HttpMethod.class), any(HttpEntity.class), eq(String.class)))
		.thenThrow(ex);

        try {
        	esService.generatePutRequest(referenceCode, route);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
    }

	@Test
    public void testGenerateGetRequest() {

        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(file, HttpStatus.OK);

        Mockito
        	.when(restTemplate.exchange(any(String.class), any(HttpMethod.class), any(HttpEntity.class), eq(byte[].class)))
        	.thenReturn(response);

        byte[] test = esService.generateGetRequest(route);
    	Assert.assertArrayEquals(response.getBody(), test);
    }

	@Test
    public void testGenerateGetRequestIfApiReturnException() {

        Mockito.when(restTemplate.exchange(any(String.class), any(HttpMethod.class), any(HttpEntity.class), eq(byte[].class)))
        		.thenThrow(RestClientException.class);

        try {
        	esService.generateGetRequest(route);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
			Assert.assertEquals(INTERNAL_SERVER_ERROR, e.getMessage());
		}
    }

	@Test
    public void testGenerateGetRequestIfApiReturnHttpClientErrorException() {

		HttpClientErrorException ex = new HttpClientErrorException(HttpStatus.BAD_REQUEST, "400", json.getBytes(), null);

        Mockito.when(utilities.stringToObject(json, YouSignErrorResponse.class))
        		.thenReturn(youSignError);

        Mockito.when(restTemplate.exchange(any(String.class), any(HttpMethod.class), any(HttpEntity.class), eq(byte[].class)))
		.thenThrow(ex);

        try {
        	esService.generateGetRequest(route);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
    }



}
