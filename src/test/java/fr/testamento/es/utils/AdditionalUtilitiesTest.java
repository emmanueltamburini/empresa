package fr.testamento.es.utils;

import static fr.testamento.es.Constants.ID_KEY;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.testamento.es.exceptions.RestException;
import fr.testamento.es.pojo.ProcedureRequestPojo;

@RunWith(SpringRunner.class)
@TestPropertySource(properties = {
		"signature.page-position=1",
		"signature.position=8,10,129,67",
		"signature.top-mention=Date: {{date}}",
		"signature.bottom-mention=Signataire: {{firstName}} {{lastName}}",
		"yousign.validation-type=email",
		"signature.image-path=src/test/test/resources/signature/sign.png",
		"yousign.validate-authentication-sms=/authentications/sms/{id}",
		"yousign.validate-authentication-email=/authentications/email/{id}",
		"yousign.validation-message= ",
		"date.format=dd/MM/yyyy",
		"datetime.format=dd/MM/yyyy HH:mm:ss"
})
public class AdditionalUtilitiesTest {

	@TestConfiguration
    static class UtilitiesConfiguration {

		@Value("${date.format}")
		private String dateFormat;

		@Value("${datetime.format}")
		private String datetimeFormat;

        @Bean
        public Utilities utilities() {
            return new Utilities(dateFormat, datetimeFormat);
        }
    }

	@Autowired
	private Utilities utilities;

	@Value("${signature.top-mention}")
	private String mention;

	@Value("${signature.bottom-mention}")
	private String mention2;

	@Value("${yousign.validation-type}")
	private String operationCustomMode;

	@Value("${signature.image-path}")
	private String imagePath;

	@Value("${yousign.validate-authentication-email}")
    private String validateEmailRoute;

    @Value("${yousign.validate-authentication-sms}")
    private String validateSmsRoute;

	private String firstName = "firstName";
	private String lastName = "lastName";
	private String phone = "firstName";
	private String email = "email";

    @Test
	public void testGetImageSignature() {

    	try {
    		utilities.getImageSignature();
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
	}

    @Test
	public void testGetAuthenticationRoute() {

		String test = utilities.getAuthenticationRoute("test");
		Assert.assertEquals(validateEmailRoute.replace(ID_KEY, "test"), test);
	}

    @Test
	public void testGenerateProcedure() {
        ProcedureRequestPojo test = utilities.generateProcedure(firstName, lastName, phone, email);

		Assert.assertEquals(firstName, test.getMembers()[0].getFirstname());
		Assert.assertEquals(lastName, test.getMembers()[0].getLastname());
		Assert.assertEquals(phone, test.getMembers()[0].getPhone());
		Assert.assertEquals(email, test.getMembers()[0].getEmail());
		Assert.assertNull(test.getMembers()[0].getOperationModeSmsConfig());
	}

}
