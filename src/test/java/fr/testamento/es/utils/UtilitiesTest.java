package fr.testamento.es.utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.testamento.es.exceptions.RestException;
import fr.testamento.es.pojo.AuthenticationResponsePojo;
import fr.testamento.es.pojo.FileObjectRequestPojo;
import fr.testamento.es.pojo.FileResponsePojo;
import fr.testamento.es.pojo.MemberPojo;
import fr.testamento.es.pojo.OperationRequestPojo;
import fr.testamento.es.pojo.OperationResponsePojo;
import fr.testamento.es.pojo.ProcedureRequestPojo;
import fr.testamento.es.pojo.ProcedureResponsePojo;

import static fr.testamento.es.Constants.*;

@RunWith(SpringRunner.class)
@TestPropertySource(properties = {
		"signature.page-position=1",
		"signature.position=8,10,129,67",
		"signature.top-mention=Date: {{date}}",
		"signature.bottom-mention=Signataire: {{firstName}} {{lastName}}",
		"yousign.validation-type=sms",
		"signature.image-path=src/test/resources/signature/sign.png",
		"yousign.validate-authentication-sms=/authentications/sms/{id}",
		"yousign.validate-authentication-email=/authentications/email/{id}",
		"date.format=dd/MM/yyyy",
		"datetime.format=dd/MM/yyyy HH:mm:ss"
})
public class UtilitiesTest {

	@TestConfiguration
    static class UtilitiesConfiguration {
		@Value("${date.format}")
		private String dateFormat;

		@Value("${datetime.format}")
		private String datetimeFormat;

        @Bean
        public Utilities utilities() {
        	return new Utilities(dateFormat, datetimeFormat);
        }
    }

	@Autowired
	private Utilities utilities;

	@Value("${signature.page-position}")
	private Integer page;

	@Value("${signature.position}")
	private String position;

	@Value("${signature.top-mention}")
	private String mention;

	@Value("${signature.bottom-mention}")
	private String mention2;

	@Value("${yousign.validation-type}")
	private String operationCustomMode;

	@Value("${signature.image-path}")
	private String imagePath;

	@Value("${yousign.validate-authentication-email}")
    private String validateEmailRoute;

    @Value("${yousign.validate-authentication-sms}")
    private String validateSmsRoute;

	private String id = "id";
	private String firstName = "firstName";
	private String lastName = "lastName";
	private String phone = "firstName";
	private String email = "email";
	private String value = "value";
	private AuthenticationResponsePojo authentication = new AuthenticationResponsePojo(value, STATUS_ACTIVE, value);
	private OperationResponsePojo operationResponse = new OperationResponsePojo();
	private String json;
	private YouSignErrorResponse youSignError = new YouSignErrorResponse(value, value, value);

	@Test
	public void testGenerateFileObject() {

		MemberPojo member = new MemberPojo();
		member.setId(id);
		member.setEmail(email);
		member.setFirstname(firstName);
		member.setLastname(lastName);
		member.setPhone(phone);

		ProcedureResponsePojo procedureResponsePojo = new ProcedureResponsePojo();
		procedureResponsePojo.setId(id);
		procedureResponsePojo.setMembers(new MemberPojo[] {member});

		FileResponsePojo fileResponse = new FileResponsePojo();
		fileResponse.setId(id);

		FileObjectRequestPojo fileObject = new FileObjectRequestPojo();
		fileObject.setMention(utilities.replaceKeys(procedureResponsePojo.getMembers()[0], mention));
		fileObject.setMention2(utilities.replaceKeys(procedureResponsePojo.getMembers()[0], mention2));
		fileObject.setPage(page);
		fileObject.setPosition(position);
		fileObject.setFile(fileResponse.getId());
		fileObject.setMember(procedureResponsePojo.getMembers()[0].getId());

		FileObjectRequestPojo fileObjectRequestPojo = utilities.generateFileObject(procedureResponsePojo, fileResponse);

		Assert.assertEquals(fileObject.getFile(), fileObjectRequestPojo.getFile());
		Assert.assertEquals(fileObject.getMember(), fileObjectRequestPojo.getMember());
		Assert.assertEquals(fileObject.getMention(), fileObjectRequestPojo.getMention());
		Assert.assertEquals(fileObject.getMention2(), fileObjectRequestPojo.getMention2());
		Assert.assertEquals(fileObject.getPosition(), fileObjectRequestPojo.getPosition());
		Assert.assertEquals(fileObject.getPage(), fileObjectRequestPojo.getPage());
	}

	@Test
	public void testGenerateProcedure() {
        ProcedureRequestPojo test = utilities.generateProcedure(firstName, lastName, phone, email);

		Assert.assertEquals(firstName, test.getMembers()[0].getFirstname());
		Assert.assertEquals(lastName, test.getMembers()[0].getLastname());
		Assert.assertEquals(phone, test.getMembers()[0].getPhone());
		Assert.assertEquals(email, test.getMembers()[0].getEmail());
	}

	@Test
	public void testGenerateOperation() {

		MemberPojo member = new MemberPojo();
		member.setId(id);

		OperationRequestPojo test = utilities.generateOperation(member);

		Assert.assertEquals(operationCustomMode, test.getMode());
		Assert.assertEquals(id, test.getMembers()[0]);
		Assert.assertEquals(DEFAULT_TYPE, test.getType());
	}

	@Test
	public void testGetFileName() {

		String test = utilities.getFileName();

		Assert.assertFalse(test.isBlank());
	}

	@Test
	public void testReplaceKeysIfMessageIsNull() {

		MemberPojo member = new MemberPojo();
		member.setId(id);
		member.setEmail(email);
		member.setFirstname(firstName);
		member.setLastname(lastName);
		member.setPhone(phone);

		String test = utilities.replaceKeys(member, null);

		Assert.assertNull(test);
	}

	@Test
	public void testReplaceKeysIfMessageIsEmpty() {

		MemberPojo member = new MemberPojo();
		member.setId(id);
		member.setEmail(email);
		member.setFirstname(firstName);
		member.setLastname(lastName);
		member.setPhone(phone);

		String test = utilities.replaceKeys(member, "");

		Assert.assertTrue(test.isEmpty());
	}

	@Test
	public void testReplaceKeysIfMemberIsNull() {

		MemberPojo member = new MemberPojo();
		member.setId(id);
		member.setEmail(email);
		member.setFirstname(firstName);
		member.setLastname(lastName);
		member.setPhone(phone);

		String test = utilities.replaceKeys(null, "test");

		Assert.assertEquals("test", test);
	}

	@Test
	public void testValidateProcedureIfProcedureIsNull() {
		try {
        	utilities.validateProcedure(null);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
	}

	@Test
	public void testValidateProcedureIfStatusIsNull() {

		MemberPojo member = new MemberPojo();
		member.setId(id);
		member.setEmail(email);
		member.setFirstname(firstName);
		member.setLastname(lastName);
		member.setPhone(phone);

		ProcedureResponsePojo procedureResponsePojo = new ProcedureResponsePojo();
		procedureResponsePojo.setId(id);
		procedureResponsePojo.setMembers(new MemberPojo[] {member});

		try {
        	utilities.validateProcedure(procedureResponsePojo);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
	}

	@Test
	public void testValidateProcedureIfStatusIsInvalid() {

		MemberPojo member = new MemberPojo();
		member.setId(id);
		member.setEmail(email);
		member.setFirstname(firstName);
		member.setLastname(lastName);
		member.setPhone(phone);

		ProcedureResponsePojo procedureResponsePojo = new ProcedureResponsePojo();
		procedureResponsePojo.setId(id);
		procedureResponsePojo.setStatus("test");
		procedureResponsePojo.setMembers(new MemberPojo[] {member});

		try {
        	utilities.validateProcedure(procedureResponsePojo);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
	}

	@Test
	public void testValidateProcedureIfMembersIsNull() {

		ProcedureResponsePojo procedureResponsePojo = new ProcedureResponsePojo();
		procedureResponsePojo.setId(id);
		procedureResponsePojo.setStatus(STATUS_ACTIVE);
		procedureResponsePojo.setMembers(null);

		try {
        	utilities.validateProcedure(procedureResponsePojo);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
	}

	@Test
	public void testValidateProcedureIfMembersIsEmpty() {

		ProcedureResponsePojo procedureResponsePojo = new ProcedureResponsePojo();
		procedureResponsePojo.setId(id);
		procedureResponsePojo.setStatus(STATUS_ACTIVE);
		procedureResponsePojo.setMembers(new MemberPojo[] {});

		try {
        	utilities.validateProcedure(procedureResponsePojo);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
	}

	@Test
	public void testValidateProcedureIfMembersIdIsNull() {

		MemberPojo member = new MemberPojo();
		member.setId(null);
		member.setEmail(email);
		member.setFirstname(firstName);
		member.setLastname(lastName);
		member.setPhone(phone);

		ProcedureResponsePojo procedureResponsePojo = new ProcedureResponsePojo();
		procedureResponsePojo.setId(id);
		procedureResponsePojo.setStatus(STATUS_ACTIVE);
		procedureResponsePojo.setMembers(new MemberPojo[] {member});

		try {
        	utilities.validateProcedure(procedureResponsePojo);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
	}

	@Test
	public void testValidateProcedure() {

		MemberPojo member = new MemberPojo();
		member.setId(id);
		member.setEmail(email);
		member.setFirstname(firstName);
		member.setLastname(lastName);
		member.setPhone(phone);

		ProcedureResponsePojo procedureResponsePojo = new ProcedureResponsePojo();
		procedureResponsePojo.setId(id);
		procedureResponsePojo.setStatus(STATUS_ACTIVE);
		procedureResponsePojo.setMembers(new MemberPojo[] {member});

		utilities.validateProcedure(procedureResponsePojo);
	}

	@Test
	public void testValidateOperation() {

		operationResponse.setAuthentication(authentication);
    	operationResponse.setId(value);
    	operationResponse.setMode(value);
    	operationResponse.setStatus(value);
    	operationResponse.setType(value);

		utilities.validateOperation(operationResponse);
	}

	@Test
	public void testValidateOperationIfOperationIsNull() {

		operationResponse.setAuthentication(authentication);
    	operationResponse.setId(value);
    	operationResponse.setMode(value);
    	operationResponse.setStatus(value);
    	operationResponse.setType(value);

    	try {
    		utilities.validateOperation(null);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
	}

	@Test
	public void testValidateOperationIfAuthenticationIsNull() {

		operationResponse.setAuthentication(null);

    	try {
    		utilities.validateOperation(operationResponse);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
	}

	@Test
	public void testValidateOperationIfStatusIsNull() {

		authentication.setStatus(null);

		operationResponse.setAuthentication(authentication);
    	operationResponse.setId(value);
    	operationResponse.setMode(value);
    	operationResponse.setStatus(value);
    	operationResponse.setType(value);

    	try {
    		utilities.validateOperation(operationResponse);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
	}

	@Test
	public void testValidateOperationIfStatusIsInvalid() {

		authentication.setStatus("test");

		operationResponse.setAuthentication(authentication);
    	operationResponse.setId(value);
    	operationResponse.setMode(value);
    	operationResponse.setStatus(value);
    	operationResponse.setType(value);

    	try {
    		utilities.validateOperation(operationResponse);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
	}

	@Test
	public void testValidateAuthentication() {
		authentication.setStatus(STATUS_USED);

		utilities.validateAuthentication(authentication);
	}

	@Test
	public void testValidateAuthenticationIfAuthenticationIsNull() {
		try {
			utilities.validateAuthentication(null);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
	}

	@Test
	public void testValidateAuthenticationIfStatusIsNull() {
		authentication.setStatus(null);

		try {
			utilities.validateAuthentication(authentication);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
	}

	@Test
	public void testValidateAuthenticationIfStatusIsInvalid() {
		authentication.setStatus("test");

		try {
			utilities.validateAuthentication(authentication);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
	}

	@Test
	public void testGetImageSignature() {

		String test = utilities.getImageSignature();
		Assert.assertFalse(test.isEmpty());
	}

	@Test
	public void testStringToObject() throws JsonProcessingException {

		json = new ObjectMapper().writeValueAsString(youSignError);

		YouSignErrorResponse test = utilities.stringToObject(json, YouSignErrorResponse.class);
		Assert.assertEquals(youSignError.getDetail(), test.getDetail());
		Assert.assertEquals(youSignError.getTitle(), test.getTitle());
		Assert.assertEquals(youSignError.getType(), test.getType());
	}

	@Test
	public void testStringToObjectIfObjValueIsInvalid(){

		try {
			utilities.stringToObject("{ \"value\": \"test\", \"value\"}", YouSignErrorResponse.class);
        } catch (RestException e) {
			Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatus());
		}
	}

	@Test
	public void testGetAuthenticationRoute() {

		String test = utilities.getAuthenticationRoute("test");
		Assert.assertEquals(validateSmsRoute.replace(ID_KEY, "test"), test);
	}

	@Test
	public void testRemoveSpecialCharactere(){
		String inputLastName = "DU!@#$P%^&*(ON)[]T<{}";
		String expectedLastName = "DUPONT";

		String actualLastName = utilities.cleanFieldData(inputLastName, FIRST_NAME);

		Assert.assertEquals(expectedLastName, actualLastName);
	}

	@Test
	public void testRemoveSpaceBeforeAndAfterField(){
		String inputLastName = " DUPONT  ";
		String expectedLastName = "DUPONT";

		String actualLastName = utilities.cleanFieldData(inputLastName, FIRST_NAME);

		Assert.assertEquals(expectedLastName, actualLastName);
	}

	@Test
	public void testReplaceCommaBySpace(){
		String inputFirstName = "Jean,Mouloud,David";
		String expectedFirstName = "Jean Mouloud David";

		String actualFirstName = utilities.cleanFieldData(inputFirstName, FIRST_NAME);

		Assert.assertEquals(expectedFirstName, actualFirstName);
	}


}
