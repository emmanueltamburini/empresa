package fr.testamento.es.utils;

import org.junit.Assert;
import org.junit.Test;

public class ResponseTest {

	private final String value = "value";

    @Test
    public void testResponseEmptyConstructor() {

    	Response response = new Response();

        Assert.assertNull(response.getAuthentication());
        Assert.assertNull(response.getFile());
        Assert.assertNull(response.getSignatory());
        Assert.assertNull(response.getStatus());
    }

    @Test
    public void testResponseGettersAndSetters() {

    	Response response = new Response();
    	response.setAuthentication(value);
    	response.setFile(value);
    	response.setSignatory(value);
    	response.setStatus(value);

    	Assert.assertEquals(value, response.getAuthentication());
    	Assert.assertEquals(value, response.getFile());
    	Assert.assertEquals(value, response.getSignatory());
    	Assert.assertEquals(value, response.getStatus());

    }

}
