package fr.testamento.es.utils;

import org.junit.Assert;
import org.junit.Test;

public class YouSignErrorResponseTest {

	private final String value = "value";

    @Test
    public void testYouSignErrorResponseEmptyConstructor() {

    	YouSignErrorResponse youSignError = new YouSignErrorResponse();

        Assert.assertNull(youSignError.getDetail());
        Assert.assertNull(youSignError.getTitle());
        Assert.assertNull(youSignError.getType());
    }

    @Test
    public void testYouSignErrorResponseGettersAndSetters() {

    	YouSignErrorResponse youSignError = new YouSignErrorResponse();
    	youSignError.setDetail(value);
    	youSignError.setTitle(value);
    	youSignError.setType(value);

    	Assert.assertEquals(value, youSignError.getDetail());
    	Assert.assertEquals(value, youSignError.getTitle());
    	Assert.assertEquals(value, youSignError.getType());

    }

    @Test
    public void testYouSignErrorResponseAllArguments() {

    	YouSignErrorResponse youSignError = new YouSignErrorResponse(value, value, value);

    	Assert.assertEquals(value, youSignError.getDetail());
    	Assert.assertEquals(value, youSignError.getTitle());
    	Assert.assertEquals(value, youSignError.getType());

    }

}
