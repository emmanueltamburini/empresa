package fr.testamento.es.utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;

import junit.framework.TestCase;

@RunWith(SpringRunner.class)
public class EsResponseTest extends TestCase {

	@Test
    public void testEsResponseEmptyConstructor() {
		EsResponse es = new EsResponse();
        Assert.assertNull(es.getMessage());
        Assert.assertEquals(0, es.getCode());
    }

    @Test
    public void testEsResponseGettersAndSetters() {

    	int code = 1;
    	String message = "message_test";
    	HttpStatus type = HttpStatus.CREATED;
    	EsResponse es = new EsResponse();

    	es.setCode(code);
    	es.setMessage(message);
    	es.setType(type);

        Assert.assertEquals(message, es.getMessage());
        Assert.assertEquals(code, es.getCode());
        Assert.assertEquals(type, es.getType());
    }

    @Test
    public void testEsResponseRequiredConstructor() {

    	int code = 1;
    	String message = "message_test";
    	HttpStatus type = HttpStatus.CREATED;
    	EsResponse es = new EsResponse(type, code, message);

    	Assert.assertEquals(message, es.getMessage());
        Assert.assertEquals(code, es.getCode());
        Assert.assertEquals(type, es.getType());
    }

    @Test
	public void testEsResponseToJson() throws JsonProcessingException {
    	int code = 400;
    	String message = "message_test";
    	HttpStatus type = HttpStatus.CREATED;
    	EsResponse es = new EsResponse(type, code, message);

    	String test = es.toJson();
    	Assert.assertFalse(test.isEmpty());
    }

}

