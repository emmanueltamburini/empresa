package fr.testamento.es.controller;

import static fr.testamento.es.Constants.AUTHENTICATION_ID;
import static fr.testamento.es.Constants.EMAIL;
import static fr.testamento.es.Constants.FILE_ID;
import static fr.testamento.es.Constants.FIRST_NAME;
import static fr.testamento.es.Constants.ID_KEY;
import static fr.testamento.es.Constants.LAST_NAME;
import static fr.testamento.es.Constants.PHONE_CODE;
import static fr.testamento.es.Constants.PHONE_NUMBER;
import static fr.testamento.es.Constants.REFERENCE_CODE;
import static fr.testamento.es.Constants.SIGNATORY_ID;
import static fr.testamento.es.Constants.STATUS_STARTED;
import static fr.testamento.es.URLConstants.API_BASE;
import static fr.testamento.es.URLConstants.API_SIGNATURE_CREATE;
import static fr.testamento.es.URLConstants.API_SIGNATURE_GENERATE;
import static fr.testamento.es.URLConstants.API_SIGNATURE_PDF;
import static fr.testamento.es.URLConstants.API_SIGNATURE_STATUS;
import static fr.testamento.es.URLConstants.ES_API_SIGNATURE_GENERATE;
import static fr.testamento.es.URLConstants.ES_API_SIGNATURE_STATUS;
import static fr.testamento.es.URLConstants.ES_API_SIGNATURE_VALIDATE;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.HttpClientErrorException;

import fr.testamento.es.exceptions.RestException;
import fr.testamento.es.pojo.AuthenticationResponsePojo;
import fr.testamento.es.pojo.FileObjectRequestPojo;
import fr.testamento.es.pojo.FileObjectResponsePojo;
import fr.testamento.es.pojo.FileRequestPojo;
import fr.testamento.es.pojo.FileResponsePojo;
import fr.testamento.es.pojo.MemberPojo;
import fr.testamento.es.pojo.OperationModeConfig;
import fr.testamento.es.pojo.OperationRequestPojo;
import fr.testamento.es.pojo.OperationResponsePojo;
import fr.testamento.es.pojo.ProcedureRequestPojo;
import fr.testamento.es.pojo.ProcedureResponsePojo;
import fr.testamento.es.security.RestAuthenticationEntryPoint;
import fr.testamento.es.service.ElectronicSignatureService;
import fr.testamento.es.utils.Utilities;
import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ElectronicSignatureController.class, secure = false)
public class ElectronicSignatureControllerTest extends TestCase {

	@Autowired
    private MockMvc mvc;

	private String phoneNumber = "testNumber";

	@MockBean
	private ElectronicSignatureService esService;

	@MockBean
    private RestAuthenticationEntryPoint authenticationEntryPoint;

	@MockBean
	private Utilities utilities;

	private String value = "test";
	private byte[] file = "test".getBytes();
	private final Integer integerValue = 1;
	private FileResponsePojo fileResponse = new FileResponsePojo();
	private final MemberPojo member = new MemberPojo();
	private ProcedureRequestPojo procedureRequest = new ProcedureRequestPojo();
	private ProcedureResponsePojo procedureResponse = new ProcedureResponsePojo();
	private FileObjectRequestPojo fileObjectRequest = new FileObjectRequestPojo();
	private FileObjectResponsePojo fileObjectResponse = new FileObjectResponsePojo();
	private OperationRequestPojo operationRequest = new OperationRequestPojo(value, value, new String[] {value} );
	private AuthenticationResponsePojo authenticationResponse = new AuthenticationResponsePojo();
	private OperationResponsePojo operationResponse = new OperationResponsePojo();
	private final OperationModeConfig operationMode = new OperationModeConfig();

	@Override
	@Before
    public void setUp() throws Exception {
        super.setUp();

        member.setEmail(value);
    	member.setFirstname(value);
    	member.setId(value);
    	member.setLastname(value);
    	member.setOperationCustomModes(new String[] {value});
    	member.setOperationModeEmailConfig(operationMode);
    	member.setOperationModeSmsConfig(operationMode);
    	member.setPhone(value);
    	member.setStatus(value);

        fileResponse.setCompany(value);
    	fileResponse.setContentType(value);
    	fileResponse.setDescription(value);
    	fileResponse.setId(value);
    	fileResponse.setName(value);
    	fileResponse.setSha256(value);
    	fileResponse.setType(value);
    	fileResponse.setWorkspace(value);

    	procedureRequest.setMembers(new MemberPojo[] {member});
    	procedureRequest.setName(value);

    	procedureResponse.setDescription(value);
    	procedureResponse.setId(value);
    	procedureResponse.setMembers(new MemberPojo[] {member});
    	procedureResponse.setName(value);
    	procedureResponse.setStatus(value);
    	procedureResponse.setWorkspace(value);

    	fileObjectRequest.setFile(value);
    	fileObjectRequest.setMember(value);
    	fileObjectRequest.setMention(value);
    	fileObjectRequest.setMention2(value);
    	fileObjectRequest.setPage(integerValue);
    	fileObjectRequest.setPosition(value);

    	fileObjectResponse.setFile(value);
    	fileObjectResponse.setMember(member);
    	fileObjectResponse.setMention(value);
    	fileObjectResponse.setMention2(value);
    	fileObjectResponse.setPage(integerValue);
    	fileObjectResponse.setPosition(value);

    	authenticationResponse.setId(value);
    	authenticationResponse.setStatus(value);
    	authenticationResponse.setType(value);

    	operationResponse.setAuthentication(authenticationResponse);
    	operationResponse.setId(value);
    	operationResponse.setMode(value);
    	operationResponse.setStatus(value);
    	operationResponse.setType(value);

    }

	@Test
    public void createSignatureTest() throws Exception {

		 mvc.perform( MockMvcRequestBuilders.multipart(API_SIGNATURE_CREATE)
				 	.param(FILE_ID, "file.pdf")
					.param(PHONE_NUMBER, phoneNumber))
		   .andExpect(status().isOk());
	}

	@Test
    public void createSignatureIfFileNullTest() throws Exception {

		 mvc.perform( MockMvcRequestBuilders.multipart(API_SIGNATURE_CREATE)
					.param(PHONE_NUMBER, phoneNumber))
		   .andExpect(status().isBadRequest());
	}

	@Test
    public void createSignatureIfPhoneNumberIsNullTest() throws Exception {

		 mvc.perform( MockMvcRequestBuilders.multipart(API_SIGNATURE_CREATE)
				 .file(FILE_ID, file))
		   .andExpect(status().isBadRequest());
	}

	@Test
    public void generateSignatureTest() throws Exception {

		mvc.perform(post(API_SIGNATURE_GENERATE)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
				.param(REFERENCE_CODE, "123")
				.param(PHONE_CODE, "321"))
		   .andExpect(status().isOk());
	}

	@Test
    public void generateSignatureIfReferenceCodeIsNullTest() throws Exception {

		mvc.perform(post(API_SIGNATURE_GENERATE)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
				.param(PHONE_NUMBER, "321"))
		   .andExpect(status().isBadRequest());
	}

	@Test
    public void generateSignatureIfPhoneCodeIsNullTest() throws Exception {

		mvc.perform(post(API_SIGNATURE_GENERATE)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
				.param(REFERENCE_CODE, "123"))
		   .andExpect(status().isBadRequest());
	}

	@Test
    public void getStatusTest() throws Exception {

		mvc.perform(get(API_SIGNATURE_STATUS + "/1")
				.accept(MediaType.APPLICATION_JSON))
		   		.andExpect(status().isOk());
	}

	@Test
    public void getFileTest() throws Exception {

		mvc.perform(get(API_SIGNATURE_PDF + "/1")
				.accept(MediaType.APPLICATION_PDF))
		   		.andExpect(status().isOk());
	}

	@Test
    public void generateYousignSignatureTest() throws Exception {

		Mockito.when(utilities.getFileName()).thenReturn(value);

		Mockito.when(esService.generatePostRequest(any(FileRequestPojo.class), any(String.class))).thenReturn(value);

		Mockito.when(utilities.stringToObject(any(String.class), eq(FileResponsePojo.class)))
			.thenReturn(fileResponse);

		Mockito.when(utilities.generateProcedure(any(String.class), any(String.class), any(String.class), any(String.class)))
			.thenReturn(procedureRequest);

		Mockito.when(utilities.stringToObject(any(String.class), eq(ProcedureResponsePojo.class)))
			.thenReturn(procedureResponse);

		Mockito.when(utilities.generateFileObject(any(ProcedureResponsePojo.class), any(FileResponsePojo.class)))
			.thenReturn(fileObjectRequest);

		Mockito.when(utilities.stringToObject(any(String.class), eq(FileObjectResponsePojo.class)))
			.thenReturn(fileObjectResponse);

		Mockito.when(utilities.generateOperation(any(MemberPojo.class)))
			.thenReturn(operationRequest);

		Mockito.when(utilities.stringToObject(any(String.class), eq(OperationResponsePojo.class)))
			.thenReturn(operationResponse);

		Mockito.when(esService.generatePostRequest(any(Object.class), any(String.class)))
			.thenReturn(value);

		Mockito.when(utilities.cleanFieldData(any(String.class), any(String.class)))
				.thenReturn(value);

		mvc.perform(post(ES_API_SIGNATURE_GENERATE)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
			 	.param(FILE_ID, "file.pdf")
				.param(FIRST_NAME, value)
				.param(LAST_NAME, value)
				.param(EMAIL, value)
				.param(PHONE_NUMBER, value))
		   .andExpect(status().isCreated())
		   .andExpect(jsonPath("$.signatory", is(value)))
		   .andExpect(jsonPath("$.authentication", is(value)))
		   .andExpect(jsonPath("$.status", is(value.toUpperCase())))
		   .andExpect(jsonPath("$.file", is(value)));
	}

	@Test
    public void validateSignatureTest() throws Exception {

		Mockito.when(utilities.getImageSignature()).thenReturn(value);

		Mockito.when(utilities.getAuthenticationRoute(any(String.class)))
			.thenReturn(value);

		Mockito.when(utilities.stringToObject(any(String.class), eq(AuthenticationResponsePojo.class)))
			.thenReturn(authenticationResponse);

		Mockito.when(esService.generatePutRequest(any(Object.class), any(String.class)))
			.thenReturn(value);

		mvc.perform(post(ES_API_SIGNATURE_VALIDATE)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
			 	.param(PHONE_CODE, value)
				.param(AUTHENTICATION_ID, value))
		   .andExpect(status().isOk())
		   .andExpect(jsonPath("$.status", is(STATUS_STARTED)));
	}

	@Test
    public void updateSignatureTest() throws Exception {

		Mockito.when(utilities.generateOperation(any(MemberPojo.class))).thenReturn(operationRequest);

		Mockito.when(utilities.stringToObject(any(String.class), eq(OperationResponsePojo.class)))
			.thenReturn(operationResponse);

		Mockito.when(esService.generatePostRequest(any(Object.class), any(String.class)))
			.thenReturn(value);

		mvc.perform(put(ES_API_SIGNATURE_GENERATE)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
			 	.param(SIGNATORY_ID, value))
		   .andExpect(status().isOk())
		   .andExpect(jsonPath("$.status", is(STATUS_STARTED)));
	}

	@Test
    public void getPdfSignatureTest() throws Exception {

		Mockito.when(utilities.getFileName()).thenReturn(value);

		Mockito.when(esService.generateGetRequest(any(String.class)))
			.thenReturn(file);

		mvc.perform(get(API_BASE + API_SIGNATURE_PDF + "/file-id")
				.accept(MediaType.APPLICATION_PDF))
   				.andExpect(status().isOk());
	}

	@Test
    public void handleHttpRequestMethodNotSupportedTest() throws Exception {

		mvc.perform(delete(ES_API_SIGNATURE_GENERATE)
					.accept(MediaType.APPLICATION_JSON))
		   .andExpect(status().isBadRequest())
		   .andExpect(jsonPath("$.message", is("La méthode de demande 'DELETE' n'est pas supportée")));
	}

	@Test
    public void handleNoHandlerFoundExceptionTest() throws Exception {

		mvc.perform(post("/api/test")
					.accept(MediaType.APPLICATION_JSON))
		   .andExpect(status().isNotFound())
		   .andExpect(jsonPath("$.message", is("Aucun gestionnaire trouvé pour POST /api/test")));
	}

	@Test
    public void validateSignatureTestIfRestExceptionIsReturn() throws Exception {

		RestException ex = new RestException(HttpStatus.NOT_FOUND, value, new String[] {value});

		Mockito.when(utilities.getImageSignature()).thenReturn(value);

		Mockito.when(utilities.getAuthenticationRoute(any(String.class)))
			.thenReturn(value);

		Mockito.when(utilities.stringToObject(any(String.class), eq(AuthenticationResponsePojo.class)))
			.thenReturn(authenticationResponse);

		Mockito.when(esService.generatePutRequest(any(Object.class), any(String.class)))
			.thenThrow(ex);

		mvc.perform(post(ES_API_SIGNATURE_VALIDATE)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
			 	.param(PHONE_CODE, value)
				.param(AUTHENTICATION_ID, value))
		   .andExpect(status().isNotFound())
		   .andExpect(jsonPath("$.message", is(value)));
	}

	@Test
    public void validateSignatureTestIfExceptionIsReturn() throws Exception {

		HttpClientErrorException ex = new HttpClientErrorException(HttpStatus.BAD_REQUEST, "400", value.getBytes(), null);

		Mockito.when(utilities.getImageSignature()).thenReturn(value);

		Mockito.when(utilities.getAuthenticationRoute(any(String.class)))
			.thenReturn(value);

		Mockito.when(utilities.stringToObject(any(String.class), eq(AuthenticationResponsePojo.class)))
			.thenReturn(authenticationResponse);

		Mockito.when(esService.generatePutRequest(any(Object.class), any(String.class)))
			.thenThrow(ex);

		mvc.perform(post(ES_API_SIGNATURE_VALIDATE)
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
			 	.param(PHONE_CODE, value)
				.param(AUTHENTICATION_ID, value))
		   .andExpect(status().isInternalServerError())
		   .andExpect(jsonPath("$.message", is("Erreur Interne du Serveur")));
	}

	@Test
    public void getSignatureStatusTest() throws Exception {


		Mockito.when(utilities.stringToObject(any(String.class), eq(MemberPojo.class)))
			.thenReturn(member);

		Mockito.when(esService.generateGetRequest(any(String.class)))
			.thenReturn(file);

		String path = ES_API_SIGNATURE_STATUS.replace(ID_KEY, value);

		mvc.perform(get(path))
		   .andExpect(status().isOk())
		   .andExpect(jsonPath("$.status", is(value.toUpperCase())));
	}

}
