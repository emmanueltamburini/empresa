package fr.testamento.es.controller;

import static fr.testamento.es.URLConstants.APP_VERSION_URL;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import fr.testamento.es.security.RestAuthenticationEntryPoint;
import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@WebMvcTest(AppController.class)
public class AppControllerTest extends TestCase {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RestAuthenticationEntryPoint authenticationEntryPoint;

    @Value("${build.version}")
    private String buildVersion;

    @Test
    public void testGetVersion() throws Exception {
        mvc.perform(get(APP_VERSION_URL)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.version", is(buildVersion)));
    }
}

