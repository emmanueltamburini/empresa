package fr.testamento.es.pojo;

import org.junit.Assert;
import org.junit.Test;

public class FileObjectRequestPojoTest {

	private final String stringValue = "value";
	private final Integer integerValue = 1;

    @Test
    public void testFileObjectRequestPojoEmptyConstructor() {

    	FileObjectRequestPojo fileObjectRequest = new FileObjectRequestPojo();

        Assert.assertNull(fileObjectRequest.getFile());
        Assert.assertNull(fileObjectRequest.getMember());
        Assert.assertNull(fileObjectRequest.getMention());
        Assert.assertNull(fileObjectRequest.getMention2());
        Assert.assertNull(fileObjectRequest.getPosition());
        Assert.assertNull(fileObjectRequest.getPage());

    }

    @Test
    public void testFileObjectRequestPojoGettersAndSetters() {

    	FileObjectRequestPojo fileObjectRequest = new FileObjectRequestPojo();

    	fileObjectRequest.setFile(stringValue);
    	fileObjectRequest.setMember(stringValue);
    	fileObjectRequest.setMention(stringValue);
    	fileObjectRequest.setMention2(stringValue);
    	fileObjectRequest.setPage(integerValue);
    	fileObjectRequest.setPosition(stringValue);

    	Assert.assertEquals(stringValue, fileObjectRequest.getFile());
    	Assert.assertEquals(stringValue, fileObjectRequest.getMember());
    	Assert.assertEquals(stringValue, fileObjectRequest.getMention());
    	Assert.assertEquals(stringValue, fileObjectRequest.getMention2());
    	Assert.assertEquals(stringValue, fileObjectRequest.getPosition());
    	Assert.assertEquals(integerValue, fileObjectRequest.getPage());

    }

}
