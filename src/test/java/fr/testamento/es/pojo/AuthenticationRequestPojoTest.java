package fr.testamento.es.pojo;

import org.junit.Assert;
import org.junit.Test;

public class AuthenticationRequestPojoTest {

    private final String value = "value";

    @Test
    public void testAuthenticationRequestPojoEmptyConstructor() {

    	AuthenticationRequestPojo authenticationRequest = new AuthenticationRequestPojo();

        Assert.assertNull(authenticationRequest.getCode());
        Assert.assertNull(authenticationRequest.getSignImage());
    }

    @Test
    public void testAuthenticationRequestPojoAllArgsConstructor() {

    	AuthenticationRequestPojo authenticationRequest = new AuthenticationRequestPojo(value, value);

        Assert.assertEquals(value, authenticationRequest.getCode());
        Assert.assertEquals(value, authenticationRequest.getSignImage());

    }

    @Test
    public void testAuthenticationRequestPojoGettersAndSetters() {

    	AuthenticationRequestPojo authenticationRequest = new AuthenticationRequestPojo();

    	authenticationRequest.setCode(value);
    	authenticationRequest.setSignImage(value);

        Assert.assertEquals(value, authenticationRequest.getCode());
        Assert.assertEquals(value, authenticationRequest.getSignImage());
    }

}
