package fr.testamento.es.pojo;

import org.junit.Assert;
import org.junit.Test;

public class MemberPojoTest {

	private final String value = "value";
	private final OperationModeConfig operationMode = new OperationModeConfig();

    @Test
    public void testMemberPojoEmptyConstructor() {

    	MemberPojo member = new MemberPojo();

        Assert.assertNull(member.getId());
        Assert.assertNull(member.getFirstname());
        Assert.assertNull(member.getLastname());
        Assert.assertNull(member.getEmail());
        Assert.assertNull(member.getPhone());
        Assert.assertNull(member.getStatus());
        Assert.assertNull(member.getOperationCustomModes());
        Assert.assertNull(member.getOperationModeEmailConfig());
        Assert.assertNull(member.getOperationModeSmsConfig());
    }

    @Test
    public void testMemberPojoGettersAndSetters() {

    	MemberPojo member = new MemberPojo();
    	member.setEmail(value);
    	member.setFirstname(value);
    	member.setId(value);
    	member.setLastname(value);
    	member.setOperationCustomModes(new String[] {value});
    	member.setOperationModeEmailConfig(operationMode);
    	member.setOperationModeSmsConfig(operationMode);
    	member.setPhone(value);
    	member.setStatus(value);

    	Assert.assertEquals(value, member.getEmail());
    	Assert.assertEquals(value, member.getFirstname());
    	Assert.assertEquals(value, member.getId());
    	Assert.assertEquals(value, member.getLastname());
    	Assert.assertEquals(value, member.getOperationCustomModes()[0]);
    	Assert.assertEquals(operationMode, member.getOperationModeEmailConfig());
    	Assert.assertEquals(operationMode, member.getOperationModeSmsConfig());
    	Assert.assertEquals(value, member.getPhone());
    	Assert.assertEquals(value, member.getStatus());

    }

}
