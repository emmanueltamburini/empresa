package fr.testamento.es.pojo;

import org.junit.Assert;
import org.junit.Test;

public class FileObjectResponsePojoTest {

	private final String stringValue = "value";
	private final Integer integerValue = 1;
	private MemberPojo member = new MemberPojo();

    @Test
    public void testFileObjectResponsePojoEmptyConstructor() {

    	FileObjectResponsePojo fileObjectResponse = new FileObjectResponsePojo();

        Assert.assertNull(fileObjectResponse.getFile());
        Assert.assertNull(fileObjectResponse.getMention());
        Assert.assertNull(fileObjectResponse.getMention2());
        Assert.assertNull(fileObjectResponse.getPosition());
        Assert.assertNull(fileObjectResponse.getMember());
        Assert.assertNull(fileObjectResponse.getPage());

    }

    @Test
    public void testFileObjectResponsePojoGettersAndSetters() {

    	FileObjectResponsePojo fileObjectResponse = new FileObjectResponsePojo();

    	fileObjectResponse.setFile(stringValue);
    	fileObjectResponse.setMember(member);
    	fileObjectResponse.setMention(stringValue);
    	fileObjectResponse.setMention2(stringValue);
    	fileObjectResponse.setPage(integerValue);
    	fileObjectResponse.setPosition(stringValue);

    	Assert.assertEquals(stringValue, fileObjectResponse.getFile());
    	Assert.assertEquals(member, fileObjectResponse.getMember());
    	Assert.assertEquals(stringValue, fileObjectResponse.getMention());
    	Assert.assertEquals(stringValue, fileObjectResponse.getMention2());
    	Assert.assertEquals(integerValue, fileObjectResponse.getPage());
    	Assert.assertEquals(stringValue, fileObjectResponse.getPosition());

    }

}
