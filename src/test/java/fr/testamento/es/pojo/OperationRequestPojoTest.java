package fr.testamento.es.pojo;

import org.junit.Assert;
import org.junit.Test;

public class OperationRequestPojoTest {

	private final String value = "value";

    @Test
    public void testOperationRequestPojoEmptyConstructor() {

    	OperationRequestPojo operationRequest = new OperationRequestPojo();

        Assert.assertNull(operationRequest.getMode());
        Assert.assertNull(operationRequest.getType());
        Assert.assertNull(operationRequest.getMembers());
    }

    @Test
    public void testOperationRequestPojoAllArgsConstructor() {

    	OperationRequestPojo operationRequest = new OperationRequestPojo(value, value, new String[] {value} );

        Assert.assertEquals(value, operationRequest.getMembers()[0]);
        Assert.assertEquals(value, operationRequest.getMode());
        Assert.assertEquals(value, operationRequest.getType());

    }

    @Test
    public void testOperationRequestPojoGettersAndSetters() {

    	OperationRequestPojo operationRequest = new OperationRequestPojo();

    	operationRequest.setMembers(new String[] {value});
    	operationRequest.setMode(value);
    	operationRequest.setType(value);

    	Assert.assertEquals(value, operationRequest.getMembers()[0]);
        Assert.assertEquals(value, operationRequest.getMode());
        Assert.assertEquals(value, operationRequest.getType());
    }

}
