package fr.testamento.es.pojo;

import org.junit.Assert;
import org.junit.Test;

public class AuthenticationResponsePojoTest {

	private final String value = "value";

    @Test
    public void testAuthenticationResponsePojoEmptyConstructor() {

    	AuthenticationResponsePojo authenticationResponse = new AuthenticationResponsePojo();

        Assert.assertNull(authenticationResponse.getId());
        Assert.assertNull(authenticationResponse.getStatus());
        Assert.assertNull(authenticationResponse.getType());
    }

    @Test
    public void testAuthenticationResponsePojoAllArgsConstructor() {

    	AuthenticationResponsePojo authenticationResponse = new AuthenticationResponsePojo(value, value, value);

        Assert.assertEquals(value, authenticationResponse.getId());
        Assert.assertEquals(value, authenticationResponse.getStatus());
        Assert.assertEquals(value, authenticationResponse.getType());

    }

    @Test
    public void testAuthenticationResponsePojoGettersAndSetters() {

    	AuthenticationResponsePojo authenticationResponse = new AuthenticationResponsePojo();

    	authenticationResponse.setId(value);
    	authenticationResponse.setStatus(value);
    	authenticationResponse.setType(value);

    	Assert.assertEquals(value, authenticationResponse.getId());
        Assert.assertEquals(value, authenticationResponse.getStatus());
        Assert.assertEquals(value, authenticationResponse.getType());
    }

}
