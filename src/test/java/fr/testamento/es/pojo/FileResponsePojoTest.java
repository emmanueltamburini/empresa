package fr.testamento.es.pojo;

import org.junit.Assert;
import org.junit.Test;

public class FileResponsePojoTest {

	private final String value = "value";

    @Test
    public void testFileResponsePojoEmptyConstructor() {

    	FileResponsePojo fileResponse = new FileResponsePojo();

        Assert.assertNull(fileResponse.getCompany());
        Assert.assertNull(fileResponse.getContentType());
        Assert.assertNull(fileResponse.getDescription());
        Assert.assertNull(fileResponse.getId());
        Assert.assertNull(fileResponse.getName());
        Assert.assertNull(fileResponse.getSha256());
        Assert.assertNull(fileResponse.getType());
        Assert.assertNull(fileResponse.getWorkspace());
    }

    @Test
    public void testFileResponsePojoGettersAndSetters() {

    	FileResponsePojo fileResponse = new FileResponsePojo();

    	fileResponse.setCompany(value);
    	fileResponse.setContentType(value);
    	fileResponse.setDescription(value);
    	fileResponse.setId(value);
    	fileResponse.setName(value);
    	fileResponse.setSha256(value);
    	fileResponse.setType(value);
    	fileResponse.setWorkspace(value);

    	Assert.assertEquals(value, fileResponse.getCompany());
    	Assert.assertEquals(value, fileResponse.getContentType());
    	Assert.assertEquals(value, fileResponse.getDescription());
    	Assert.assertEquals(value, fileResponse.getId());
    	Assert.assertEquals(value, fileResponse.getName());
    	Assert.assertEquals(value, fileResponse.getSha256());
    	Assert.assertEquals(value, fileResponse.getType());
    	Assert.assertEquals(value, fileResponse.getWorkspace());

    }

}
