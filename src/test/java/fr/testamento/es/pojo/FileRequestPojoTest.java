package fr.testamento.es.pojo;

import org.junit.Assert;
import org.junit.Test;

public class FileRequestPojoTest {

	private final String value = "value";

    @Test
    public void testFileRequestPojoEmptyConstructor() {

    	FileRequestPojo fileRequest = new FileRequestPojo();

        Assert.assertNull(fileRequest.getContent());
        Assert.assertNull(fileRequest.getName());
    }

    @Test
    public void testFileRequestPojoAllArgsConstructor() {

    	FileRequestPojo fileRequest = new FileRequestPojo(value, value);

        Assert.assertEquals(value, fileRequest.getContent());
        Assert.assertEquals(value, fileRequest.getName());

    }

    @Test
    public void testFileRequestPojoGettersAndSetters() {

    	FileRequestPojo fileRequest = new FileRequestPojo();

    	fileRequest.setContent(value);
    	fileRequest.setName(value);

    	Assert.assertEquals(value, fileRequest.getContent());
        Assert.assertEquals(value, fileRequest.getName());
    }

}
