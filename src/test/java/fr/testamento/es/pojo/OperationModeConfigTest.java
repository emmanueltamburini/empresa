package fr.testamento.es.pojo;

import org.junit.Assert;
import org.junit.Test;

public class OperationModeConfigTest {

	private final String value = "value";

    @Test
    public void testOperationModeConfigEmptyConstructor() {

    	OperationModeConfig operationModeConfig = new OperationModeConfig();

        Assert.assertNull(operationModeConfig.getContent());
    }

    @Test
    public void testOperationModeConfigAllArgsConstructor() {

    	OperationModeConfig operationModeConfig = new OperationModeConfig(value);

        Assert.assertEquals(value, operationModeConfig.getContent());

    }

    @Test
    public void testOperationModeConfigGettersAndSetters() {

    	OperationModeConfig operationModeConfig = new OperationModeConfig();

    	operationModeConfig.setContent(value);

    	Assert.assertEquals(value, operationModeConfig.getContent());
    }

}
