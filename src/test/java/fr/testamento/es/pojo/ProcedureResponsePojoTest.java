package fr.testamento.es.pojo;

import org.junit.Assert;
import org.junit.Test;

public class ProcedureResponsePojoTest {

	private final String value = "value";
	private final MemberPojo member = new MemberPojo();

    @Test
    public void testProcedureResponsePojoEmptyConstructor() {

    	ProcedureResponsePojo procedureResponse = new ProcedureResponsePojo();

        Assert.assertNull(procedureResponse.getName());
        Assert.assertNull(procedureResponse.getMembers());
        Assert.assertNull(procedureResponse.getId());
        Assert.assertNull(procedureResponse.getDescription());
        Assert.assertNull(procedureResponse.getStatus());
        Assert.assertNull(procedureResponse.getWorkspace());
    }

    @Test
    public void testProcedureResponsePojoGettersAndSetters() {

    	ProcedureResponsePojo procedureResponse = new ProcedureResponsePojo();

    	procedureResponse.setDescription(value);
    	procedureResponse.setId(value);
    	procedureResponse.setMembers(new MemberPojo[] {member});
    	procedureResponse.setName(value);
    	procedureResponse.setStatus(value);
    	procedureResponse.setWorkspace(value);

        Assert.assertEquals(member, procedureResponse.getMembers()[0]);
        Assert.assertEquals(value, procedureResponse.getName());
        Assert.assertEquals(value, procedureResponse.getDescription());
        Assert.assertEquals(value, procedureResponse.getStatus());
        Assert.assertEquals(value, procedureResponse.getWorkspace());
        Assert.assertEquals(value, procedureResponse.getId());
    }

}
