package fr.testamento.es.pojo;

import org.junit.Assert;
import org.junit.Test;

public class OperationResponsePojoTest {

	private final String value = "value";
	private final AuthenticationResponsePojo authentication = new AuthenticationResponsePojo();

    @Test
    public void testOperationResponsePojoEmptyConstructor() {

    	OperationResponsePojo operationResponse = new OperationResponsePojo();

        Assert.assertNull(operationResponse.getId());
        Assert.assertNull(operationResponse.getMode());
        Assert.assertNull(operationResponse.getStatus());
        Assert.assertNull(operationResponse.getType());
        Assert.assertNull(operationResponse.getAuthentication());
    }

    @Test
    public void testOperationResponsePojoGettersAndSetters() {

    	OperationResponsePojo operationResponse = new OperationResponsePojo();

    	operationResponse.setAuthentication(authentication);
    	operationResponse.setId(value);
    	operationResponse.setMode(value);
    	operationResponse.setStatus(value);
    	operationResponse.setType(value);

    	Assert.assertEquals(authentication, operationResponse.getAuthentication());
        Assert.assertEquals(value, operationResponse.getMode());
        Assert.assertEquals(value, operationResponse.getType());
        Assert.assertEquals(value, operationResponse.getStatus());
        Assert.assertEquals(value, operationResponse.getId());
    }

}
