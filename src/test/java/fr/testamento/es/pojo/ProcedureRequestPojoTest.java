package fr.testamento.es.pojo;

import org.junit.Assert;
import org.junit.Test;

public class ProcedureRequestPojoTest {

	private final String value = "value";
	private final MemberPojo member = new MemberPojo();

    @Test
    public void testProcedureRequestPojoEmptyConstructor() {

    	ProcedureRequestPojo procedureRequest = new ProcedureRequestPojo();

        Assert.assertNull(procedureRequest.getName());
        Assert.assertNull(procedureRequest.getMembers());
    }

    @Test
    public void testProcedureRequestPojoGettersAndSetters() {

    	ProcedureRequestPojo procedureRequest = new ProcedureRequestPojo();

    	procedureRequest.setMembers(new MemberPojo[] {member});
    	procedureRequest.setName(value);

        Assert.assertEquals(member, procedureRequest.getMembers()[0]);
        Assert.assertEquals(value, procedureRequest.getName());
    }

    @Test
    public void testProcedureRequestPojoAllArgumentsConstructor() {

    	ProcedureRequestPojo procedureRequest = new ProcedureRequestPojo(value, new MemberPojo[] {member});

        Assert.assertEquals(member, procedureRequest.getMembers()[0]);
        Assert.assertEquals(value, procedureRequest.getName());
    }

}
