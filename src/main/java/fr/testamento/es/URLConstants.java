package fr.testamento.es;

public class URLConstants {

	public static final String API_BASE = "/es";
    public static final String HEALTH_ENDPOINT_URL = "/health";
    public static final String API_URL = "/api";
    public static final String APP_VERSION_URL = API_URL + "/version";

    public static final String API_SIGNATURE_URL = API_URL + "/signature";
    public static final String API_SIGNATURE_CREATE = API_SIGNATURE_URL + "/create";
    public static final String API_SIGNATURE_GENERATE = API_SIGNATURE_URL + "/generate";
    public static final String API_SIGNATURE_STATUS = API_SIGNATURE_URL + "/status";
    public static final String API_SIGNATURE_STATUS_WITH_ID = API_SIGNATURE_STATUS + "/{reference}";
    public static final String API_SIGNATURE_PDF = API_SIGNATURE_URL + "/pdf";
    public static final String API_SIGNATURE_PDF_WITH_ID = API_SIGNATURE_PDF + "/{reference}";

    // New Implementation (Temporaly)
    public static final String ES_API_SIGNATURE_GENERATE = API_BASE + API_SIGNATURE_GENERATE;
    public static final String ES_API_SIGNATURE_VALIDATE =  API_BASE + API_SIGNATURE_URL + "/validate";
    public static final String ES_API_SIGNATURE_STATUS = API_BASE + API_SIGNATURE_STATUS + "/{id}";
    public static final String ES_API_SIGNATURE_DOWNLOAD = API_BASE + API_SIGNATURE_PDF + "/{id}";

    private URLConstants() { /* empty */}
}
