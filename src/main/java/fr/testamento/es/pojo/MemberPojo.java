package fr.testamento.es.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberPojo {

	private String id;

	private String firstname;

	private String lastname;

	private String email;

	private String phone;

	private String status;

	private String[] operationCustomModes;

	private OperationModeConfig operationModeEmailConfig;

	private OperationModeConfig operationModeSmsConfig;


}
