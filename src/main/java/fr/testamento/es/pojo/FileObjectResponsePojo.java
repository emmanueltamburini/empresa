package fr.testamento.es.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class FileObjectResponsePojo {

	private String file;

	private MemberPojo member;

	private Integer page;

	private String position;

	private String mention;

	private String mention2;

}
