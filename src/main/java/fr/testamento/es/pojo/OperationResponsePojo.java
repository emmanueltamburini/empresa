package fr.testamento.es.pojo;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class OperationResponsePojo {

    private String id;

    private AuthenticationResponsePojo authentication;

    private String mode;

    private String status;

    private String type;
}
