package fr.testamento.es.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class FileResponsePojo {

	private String id;

	private String name;

	private String type;

	private String contentType;

	private String description;

	private String sha256;

	private String workspace;

	private String company;


}
