package fr.testamento.es.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcedureResponsePojo {

	private String id;

	private String name;

	private String description;

	private String status;

	private String workspace;

	private MemberPojo[] members;

}
