package fr.testamento.es.pojo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FileObjectRequestPojo {

	private String file;

	private String member;

	private Integer page;

	private String position;

	private String mention;

	private String mention2;

}
