package fr.testamento.es.pojo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OperationRequestPojo {

    private String mode;

    private String type;

    private String[] members;
}
