package fr.testamento.es.exceptions;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public class RestException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final HttpStatus status;
    private final String[] args;

    public RestException(HttpStatus status, String message) {
        super(message);
        this.status = status;
        this.args = new String[]{};
    }

    public RestException(HttpStatus status, String message, String[] args) {
        super(message);
        this.status = status;
        this.args = args;
    }
}

