package fr.testamento.es;

public class Constants {

	// PARAMETERS
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String EMAIL = "email";
	public static final String PHONE_NUMBER = "phoneNumber";
	public static final String PHONE_CODE = "phoneCode";
	public static final String FILE_ID = "file";
	public static final String SIGNATORY_ID = "signatory";
	public static final String AUTHENTICATION_ID = "authentication";
	public static final String DATE = "date";

	// CONSTANS
	public static final String ID_KEY = "{id}";
	public static final String DOCUMENT_FORMAT = ".pdf";
	public static final String DEFAULT_VALIDATION_MODE = "sms";
	public static final String DEFAULT_TYPE = "accept";
	public static final String STATUS_ACTIVE = "active";
	public static final String STATUS_USED = "used";
	public static final String STATUS_REFUSED = "REFUSED";
	public static final String STATUS_STARTED = "PROCESSING";
	public static final String STATUS_COMPLETE = "SIGNED";
	public static final String FILE_CONSTANTS = "/files/";
	public static final String MEMBER_CONSTANTS = "/members/";

	// PARAMETERS KEY
	public static final String FIRST_NAME_KEY = "{{firstName}}";
	public static final String LAST_NAME_KEY = "{{lastName}}";
	public static final String DATE_KEY = "{{date}}";
	public static final String DATE_TIME_KEY = "{{datetime}}";
	public static final String EMAIL_KEY = "{{email}}";
	public static final String PHONE_NUMBER_KEY = "{{phoneNumber}}";

	// TO DELETE
	public static final String REFERENCE_CODE = "reference";
	public static final String STATUS_KEY = "status";

    private Constants() { /* empty */}
}
