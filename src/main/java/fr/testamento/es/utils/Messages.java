package fr.testamento.es.utils;

public class Messages {

	//Exception
	public static final String PDF_RETREAVE_ERROR = "error.retreaving_pdf";
	public static final String INTERNAL_SERVER_ERROR = "exception.internal_server_error";

	public static final String ERROR_GENERATE_SIGNATURE = "error.generate_signature";
	public static final String ERROR_GENERATE_PROCEDURE = "error.generate_procedure";
	public static final String ERROR_GENERATE_OPERATION = "error.generate_operation";
	public static final String ERROR_VALIDATE_SIGNATURE = "error.generate_validation";

	public static final String ERROR_RESPONSE_YOUSIGN = "exception.error_yousign";

	public static final String ACCESS_DENIED = "exception.authentication_required";

	private Messages(){throw new IllegalStateException("Utility class");}
}
