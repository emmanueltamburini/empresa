package fr.testamento.es.utils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Response {

	private String file;

	private String signatory;

	private String authentication;

	private String status;

}
