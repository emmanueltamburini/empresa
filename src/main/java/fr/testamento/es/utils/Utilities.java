package fr.testamento.es.utils;

import static fr.testamento.es.Constants.DATE_KEY;
import static fr.testamento.es.Constants.DATE_TIME_KEY;
import static fr.testamento.es.Constants.DEFAULT_TYPE;
import static fr.testamento.es.Constants.DEFAULT_VALIDATION_MODE;
import static fr.testamento.es.Constants.DOCUMENT_FORMAT;
import static fr.testamento.es.Constants.EMAIL_KEY;
import static fr.testamento.es.Constants.FIRST_NAME_KEY;
import static fr.testamento.es.Constants.ID_KEY;
import static fr.testamento.es.Constants.LAST_NAME_KEY;
import static fr.testamento.es.Constants.PHONE_NUMBER_KEY;
import static fr.testamento.es.Constants.STATUS_ACTIVE;
import static fr.testamento.es.Constants.STATUS_USED;
import static fr.testamento.es.utils.Messages.ERROR_GENERATE_OPERATION;
import static fr.testamento.es.utils.Messages.ERROR_GENERATE_PROCEDURE;
import static fr.testamento.es.utils.Messages.ERROR_GENERATE_SIGNATURE;
import static fr.testamento.es.utils.Messages.ERROR_VALIDATE_SIGNATURE;
import static fr.testamento.es.utils.Messages.INTERNAL_SERVER_ERROR;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.UUID;
import fr.testamento.es.Constants;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.testamento.es.exceptions.RestException;
import fr.testamento.es.pojo.AuthenticationResponsePojo;
import fr.testamento.es.pojo.FileObjectRequestPojo;
import fr.testamento.es.pojo.FileResponsePojo;
import fr.testamento.es.pojo.MemberPojo;
import fr.testamento.es.pojo.OperationModeConfig;
import fr.testamento.es.pojo.OperationRequestPojo;
import fr.testamento.es.pojo.OperationResponsePojo;
import fr.testamento.es.pojo.ProcedureRequestPojo;
import fr.testamento.es.pojo.ProcedureResponsePojo;

@Component
public class Utilities {

	private static final Logger logger = LoggerFactory.getLogger(Utilities.class);

	@Value("${signature.page-position}")
	private Integer page;

	@Value("${signature.position}")
	private String position;

	@Value("${signature.top-mention}")
	private String mention;

	@Value("${signature.bottom-mention}")
	private String mention2;

	@Value("${yousign.validation-type}")
	private String operationCustomMode;

	@Value("${yousign.validation-message}")
	private String content;

	@Value("${signature.image-path}")
	private String imagePath;

	@Value("${yousign.validate-authentication-email}")
	private String validateEmailRoute;

	@Value("${yousign.validate-authentication-sms}")
	private String validateSmsRoute;

	private DateTimeFormatter formatters;

	private DateTimeFormatter formattersDateTime;

	@Autowired
	public Utilities(@Value("${date.format}") String dateFormat,
					 @Value("${datetime.format}") String dateTimeFormat) {
		this.formatters = DateTimeFormatter.ofPattern(dateFormat);
		this.formattersDateTime = DateTimeFormatter.ofPattern(dateTimeFormat);
	}

	public ProcedureRequestPojo generateProcedure(String firstName, String lastName, String phone, String email) {

		String message = (content == null || content.isBlank()) ? null : content;
		String validationMode = this.getValidationMode();

		MemberPojo member = new MemberPojo();
		member.setEmail(email);
		member.setFirstname(firstName);
		member.setLastname(lastName);
		member.setPhone(phone);
		member.setOperationCustomModes(new String[]{validationMode});

		if (message != null) {
			String messageCompleted = this.replaceKeys(member, message);

			member.setOperationModeSmsConfig(new OperationModeConfig(messageCompleted));
			member.setOperationModeEmailConfig(new OperationModeConfig(messageCompleted));
		}

		ProcedureRequestPojo procedure = new ProcedureRequestPojo();
		procedure.setName(String.valueOf(UUID.randomUUID()));
		procedure.setMembers(new MemberPojo[]{member});

		return procedure;

	}

	public FileObjectRequestPojo generateFileObject(ProcedureResponsePojo procedureResponse, FileResponsePojo fileResponse) {

		FileObjectRequestPojo fileObjects = new FileObjectRequestPojo();
		fileObjects.setMention(this.replaceKeys(procedureResponse.getMembers()[0], mention));
		fileObjects.setMention2(this.replaceKeys(procedureResponse.getMembers()[0], mention2));
		fileObjects.setPage(page);
		fileObjects.setPosition(position);
		fileObjects.setFile(fileResponse.getId());
		fileObjects.setMember(procedureResponse.getMembers()[0].getId());

		return fileObjects;
	}

	public OperationRequestPojo generateOperation(MemberPojo memberPojo) {

		OperationRequestPojo operation = new OperationRequestPojo();
		operation.setMode(this.getValidationMode());
		operation.setType(DEFAULT_TYPE);
		operation.setMembers(new String[]{memberPojo.getId()});

		return operation;
	}

	public String getFileName() {

		return String.valueOf(UUID.randomUUID()) + DOCUMENT_FORMAT;
	}

	public String getImageSignature() {

		String encodedImage = null;
		if (imagePath != null && !imagePath.isBlank()) {
			try {
				byte[] fileContent = FileUtils.readFileToByteArray(new File(imagePath));
				encodedImage = Base64.getEncoder().encodeToString(fileContent);
			} catch (Exception e) {
				logger.error("Error loading  signature image: {}", e.getMessage());

				throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR);
			}
		}

		return encodedImage;
	}

	public String getAuthenticationRoute(String authentication) {

		return this.getValidationMode().equalsIgnoreCase(DEFAULT_VALIDATION_MODE) ?
				validateSmsRoute.replace(ID_KEY, authentication) :
				validateEmailRoute.replace(ID_KEY, authentication);

	}

	public void validateProcedure(ProcedureResponsePojo procedureResponse) {
		if (procedureResponse == null ||
				procedureResponse.getStatus() == null ||
				!procedureResponse.getStatus().equalsIgnoreCase(STATUS_ACTIVE) ||
				procedureResponse.getMembers() == null ||
				procedureResponse.getMembers().length == 0 ||
				procedureResponse.getMembers()[0].getId() == null) {
			logger.error("An error occurred while generating the procedure");

			throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, ERROR_GENERATE_SIGNATURE,
					new String[]{ERROR_GENERATE_PROCEDURE});
		}

	}

	public void validateOperation(OperationResponsePojo operationResponse) {
		if (operationResponse == null ||
				operationResponse.getAuthentication() == null ||
				operationResponse.getAuthentication().getStatus() == null ||
				!operationResponse.getAuthentication().getStatus().equalsIgnoreCase(STATUS_ACTIVE)) {
			logger.error("An error occurred while generating the validation operation");

			throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, ERROR_GENERATE_SIGNATURE,
					new String[]{ERROR_GENERATE_OPERATION});
		}

	}

	public void validateAuthentication(AuthenticationResponsePojo authenticationResponse) {
		if (authenticationResponse == null ||
				authenticationResponse.getStatus() == null ||
				!authenticationResponse.getStatus().equalsIgnoreCase(STATUS_USED)) {
			logger.error("An error occurred while validating the otp code");

			throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, ERROR_GENERATE_SIGNATURE,
					new String[]{ERROR_VALIDATE_SIGNATURE});
		}

	}

	public String replaceKeys(MemberPojo member, String message) {
		if (message != null && !message.isBlank() && member != null) {
			return message.replace(FIRST_NAME_KEY, member.getFirstname())
					.replace(LAST_NAME_KEY, member.getLastname())
					.replace(DATE_KEY, LocalDate.now().format(formatters))
					.replace(DATE_TIME_KEY, LocalDateTime.now().format(formattersDateTime))
					.replace(EMAIL_KEY, member.getEmail())
					.replace(PHONE_NUMBER_KEY, member.getPhone());
		}

		return message;

	}

	public String getValidationMode() {
		return (operationCustomMode == null || operationCustomMode.isBlank()) ?
				DEFAULT_VALIDATION_MODE : operationCustomMode.toLowerCase();
	}

	public <T> T stringToObject(String objValue, Class<T> obj) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.findAndRegisterModules();
			return objectMapper.readValue(objValue, obj);
		} catch (java.io.IOException e) {
			logger.error("JSON '{}' reading failed: {}", obj, objValue);
			logger.error(e.getMessage());
			throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR);
		}
	}

	public String cleanFieldData(String dataItem, String typeField) {

		/* Special characters escaped !@#$%^&*()[]{} */
		dataItem = dataItem.replaceAll("[\\!\\\\\"\\#\\$\\%\\&\\'\\(\\)\\*\\+\\-\\.\\/\\:\\;\\<\\=\\>\\?\\@\\[\\]\\^\\_\\`\\{\\|\\}\\~]","");

		if(typeField.equals(Constants.FIRST_NAME)) {
				dataItem = dataItem.replaceAll(",", " ");
		}
		dataItem = dataItem.trim();

		return dataItem;
	}

}
