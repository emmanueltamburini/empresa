package fr.testamento.es.controller;

import static fr.testamento.es.Constants.AUTHENTICATION_ID;
import static fr.testamento.es.Constants.EMAIL;
import static fr.testamento.es.Constants.FILE_CONSTANTS;
import static fr.testamento.es.Constants.FILE_ID;
import static fr.testamento.es.Constants.FIRST_NAME;
import static fr.testamento.es.Constants.ID_KEY;
import static fr.testamento.es.Constants.LAST_NAME;
import static fr.testamento.es.Constants.MEMBER_CONSTANTS;
import static fr.testamento.es.Constants.PHONE_CODE;
import static fr.testamento.es.Constants.PHONE_NUMBER;
import static fr.testamento.es.Constants.REFERENCE_CODE;
import static fr.testamento.es.Constants.SIGNATORY_ID;
import static fr.testamento.es.Constants.STATUS_COMPLETE;
import static fr.testamento.es.Constants.STATUS_KEY;
import static fr.testamento.es.Constants.STATUS_STARTED;
import static fr.testamento.es.URLConstants.API_SIGNATURE_CREATE;
import static fr.testamento.es.URLConstants.API_SIGNATURE_GENERATE;
import static fr.testamento.es.URLConstants.API_SIGNATURE_PDF_WITH_ID;
import static fr.testamento.es.URLConstants.API_SIGNATURE_STATUS_WITH_ID;
import static fr.testamento.es.URLConstants.ES_API_SIGNATURE_DOWNLOAD;
import static fr.testamento.es.URLConstants.ES_API_SIGNATURE_GENERATE;
import static fr.testamento.es.URLConstants.ES_API_SIGNATURE_STATUS;
import static fr.testamento.es.URLConstants.ES_API_SIGNATURE_VALIDATE;
import static fr.testamento.es.utils.Messages.PDF_RETREAVE_ERROR;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

import fr.testamento.es.exceptions.RestException;
import fr.testamento.es.pojo.AuthenticationRequestPojo;
import fr.testamento.es.pojo.AuthenticationResponsePojo;
import fr.testamento.es.pojo.FileObjectRequestPojo;
import fr.testamento.es.pojo.FileObjectResponsePojo;
import fr.testamento.es.pojo.FileRequestPojo;
import fr.testamento.es.pojo.FileResponsePojo;
import fr.testamento.es.pojo.MemberPojo;
import fr.testamento.es.pojo.OperationRequestPojo;
import fr.testamento.es.pojo.OperationResponsePojo;
import fr.testamento.es.pojo.ProcedureRequestPojo;
import fr.testamento.es.pojo.ProcedureResponsePojo;
import fr.testamento.es.service.ElectronicSignatureService;
import fr.testamento.es.utils.Response;
import fr.testamento.es.utils.Utilities;

@Controller
public class ElectronicSignatureController {

	private static final Logger logger = LoggerFactory.getLogger(ElectronicSignatureController.class);

	@Autowired
	private ElectronicSignatureService esService;

	@Autowired
	private Utilities utilities;

	@InitBinder
	public void allowEmptyDateBinding(WebDataBinder binder) {
		binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
	}

	@Value("${yousign.upload-files}")
	private String uploadFileRoute;

	@Value("${yousign.create-procedure}")
	private String generateProcedureRoute;

	@Value("${yousign.signature-properties}")
	private String setSignaturePropertiesRoute;

	@Value("${yousign.create-operation}")
	private String generateOperationRoute;

	@Value("${yousign.download-document}")
	private String downloadFileRoute;

	@Value("${yousign.signature-status}")
	private String signatureStatusRoute;

	@PostMapping(API_SIGNATURE_CREATE)
	@ResponseBody
	public ResponseEntity<Object> createSignature(@RequestParam(value = PHONE_NUMBER, required = true) String phoneNumber,
												  @RequestParam(value = FILE_ID, required = true) byte[] pdf) {

		logger.info("Called resource: createSignature");

		Map<String,String> response = new HashMap<>();

		response.put(REFERENCE_CODE, RandomStringUtils.random(8, true, true));

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping(API_SIGNATURE_GENERATE)
	@ResponseBody
	public ResponseEntity<Object> generateSignatureMock(@RequestParam(value = REFERENCE_CODE, required = true) String referenceCode,
													@RequestParam(value = PHONE_CODE, required = true) String phoneCode) {

		logger.info("Called resource: generateSignature");

		Map<String,String> response = new HashMap<>();

		response.put(STATUS_KEY, STATUS_STARTED);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping(API_SIGNATURE_STATUS_WITH_ID)
	@ResponseBody
	public ResponseEntity<Object> getStatus(@PathVariable(value = REFERENCE_CODE , required = true) String referenceCode) {

		logger.info("Called resource: getSignature");

		Map<String,String> response = new HashMap<>();

		response.put(STATUS_KEY, STATUS_COMPLETE);
		response.put(REFERENCE_CODE, RandomStringUtils.random(8, true, true));

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping( path = {API_SIGNATURE_PDF_WITH_ID}, produces = {"application/pdf"})
	@ResponseBody
	public ResponseEntity<byte[]> getSignature(@PathVariable(value = REFERENCE_CODE , required = true) String referenceCode) {

		logger.info("Called resource: getFile");

		try {

			ClassPathResource pdfFile = new ClassPathResource("static/test.pdf");

			return new ResponseEntity<>(pdfFile.getInputStream().readAllBytes(), HttpStatus.OK);
		} catch (Exception e) {
			logger.error("There was an error retrieving PDF File");
			throw new RestException(HttpStatus.BAD_REQUEST, PDF_RETREAVE_ERROR);
		}
	}

	// New Implementation
	@PostMapping(ES_API_SIGNATURE_GENERATE)
	@ResponseBody
	public ResponseEntity<Response> generateSignature(@RequestParam(value = FIRST_NAME, required = true) String firstName,
													  @RequestParam(value = LAST_NAME, required = true) String lastName,
													  @RequestParam(value = EMAIL, required = true) String email,
													  @RequestParam(value = PHONE_NUMBER, required = true) String phone,
												  	  @RequestParam(value = FILE_ID, required = true) byte[] pdf) {
        logger.info("Clean input datas");
		firstName = utilities.cleanFieldData(firstName, FIRST_NAME);
		lastName = utilities.cleanFieldData(lastName, LAST_NAME);

        logger.info("Called resource: generateSignature");
		String map;
		logger.info("Reading pdf document in base 64 format");
		String encodedFile = Base64.getEncoder().encodeToString(pdf);
		String fileName= utilities.getFileName();

		logger.info("Uploading the document to the workspace");
		FileRequestPojo fileRequest = new FileRequestPojo(fileName, encodedFile);
		map = esService.generatePostRequest(fileRequest, uploadFileRoute);
		FileResponsePojo fileResponse = utilities.stringToObject(map, FileResponsePojo.class);
		logger.info("The document was successfully uploaded to the workspace with the id of: {}", fileResponse.getId());

		logger.info("Generating a  procedure for the signatory: {} {}", firstName, lastName);
		ProcedureRequestPojo procedure = utilities.generateProcedure(firstName, lastName, phone, email);
		map = esService.generatePostRequest(procedure, generateProcedureRoute);
		ProcedureResponsePojo procedureResponse = utilities.stringToObject(map, ProcedureResponsePojo.class);
		utilities.validateProcedure(procedureResponse);
		logger.info("The procedure was successfully generated");

		logger.info("Assigning signature properties to the procedure: {}", procedureResponse.getId());
		FileObjectRequestPojo fileObject = utilities.generateFileObject(procedureResponse, fileResponse);
		map = esService.generatePostRequest(fileObject, setSignaturePropertiesRoute);
		FileObjectResponsePojo fileObjectResponse = utilities.stringToObject(map, FileObjectResponsePojo.class);
		logger.info("The properties of the signature were successfully assigned");

		logger.info("Initiating validation of the signatory's procedure: {}", fileObjectResponse.getMember().getId());
		OperationRequestPojo operation = utilities.generateOperation(fileObjectResponse.getMember());
		map =  esService.generatePostRequest(operation, generateOperationRoute);
		OperationResponsePojo operationResponse = utilities.stringToObject(map, OperationResponsePojo.class);
		utilities.validateOperation(operationResponse);
		logger.info("A OTP code has been successfully sent to the signer");

		Response response = new Response();
		response.setAuthentication(operationResponse.getAuthentication().getId());
		response.setFile(fileObjectResponse.getFile().replace(FILE_CONSTANTS, ""));
		response.setSignatory(fileObjectResponse.getMember().getId().replace(MEMBER_CONSTANTS, ""));
		response.setStatus(fileObjectResponse.getMember().getStatus().toUpperCase());

		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	@PostMapping(ES_API_SIGNATURE_VALIDATE)
	@ResponseBody
	public ResponseEntity<Response> validateSignature(@RequestParam(value = PHONE_CODE, required = true) String phoneCode,
													  @RequestParam(value = AUTHENTICATION_ID, required = true) String authentication) {

		logger.info("Called resource: validateSignature");
		String map;

		logger.info("Loading image for signature");
		String signImage = utilities.getImageSignature();

		logger.info("Validation for authentication: {} has started", authentication);
		AuthenticationRequestPojo authenticationRequest = new AuthenticationRequestPojo(phoneCode, signImage);
		String route = utilities.getAuthenticationRoute(authentication);

		map = esService.generatePutRequest(authenticationRequest, route);
		AuthenticationResponsePojo authenticationResponse = utilities.stringToObject(map, AuthenticationResponsePojo.class);
		utilities.validateAuthentication(authenticationResponse);
		logger.info("The signature has been successfully validated");

		Response response = new Response();
		response.setStatus(STATUS_STARTED);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping( path = {ES_API_SIGNATURE_DOWNLOAD},
				produces = {MediaType.APPLICATION_PDF_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<byte[]> getPdfSignature(@PathVariable("id") String file) {

		logger.info("Called resource: getPdfSignature");
		HttpHeaders headers = new HttpHeaders();
		String route = downloadFileRoute.replace(ID_KEY, file);

		logger.info("Starting document download: {}", file);
		byte[] response = esService.generateGetRequest(route);
		logger.info("The document was successfully downloaded");

		headers.add("Content-Disposition", "attachment; filename=" + utilities.getFileName());
		headers.add("Content-Transfer-Encoding", "binary");

	    return ResponseEntity.ok()
	        		.headers(headers)
					.contentType(MediaType.APPLICATION_PDF)
					.body(response);
	}

	@PutMapping(ES_API_SIGNATURE_GENERATE)
	@ResponseBody
	public ResponseEntity<Response> updateSignature(@RequestParam(value = SIGNATORY_ID, required = true) String signatory) {

		logger.info("Called resource: validateSignature");
		String map;
		MemberPojo member = new MemberPojo();
		member.setId(MEMBER_CONSTANTS + signatory);

		logger.info("Initiating validation of the signatory's procedure: {}", signatory);
		OperationRequestPojo operation = utilities.generateOperation(member);
		map =  esService.generatePostRequest(operation, generateOperationRoute);
		OperationResponsePojo operationResponse = utilities.stringToObject(map, OperationResponsePojo.class);
		utilities.validateOperation(operationResponse);
		logger.info("A OTP code has been successfully sent to the signer");

		Response response = new Response();
		response.setAuthentication(operationResponse.getAuthentication().getId());
		response.setStatus(STATUS_STARTED);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping(ES_API_SIGNATURE_STATUS)
	@ResponseBody
	public ResponseEntity<Response> getStatusBySigner(@PathVariable("id") String signatory) {

		logger.info("Called resource: getStatusSignature");
		String route = signatureStatusRoute.replace(ID_KEY, signatory);

		logger.info("Recovering the status of the member: {}", signatory);
		byte[] memberResponse = esService.generateGetRequest(route);
		MemberPojo signatureStatusResponse = utilities.stringToObject(new String(memberResponse), MemberPojo.class);
		logger.info("The status has been successfully recovered");

		Response response = new Response();
		response.setStatus(signatureStatusResponse.getStatus().toUpperCase());

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
