package fr.testamento.es.security;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import fr.testamento.es.utils.EsResponse;

@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {

	private static final Logger logger = LoggerFactory.getLogger(RestAuthenticationEntryPoint.class);

	private static final String PREFIX_ERROR_MESSAGE = "Exception message: {}";

    @Autowired
    private MessageSource messageSource;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
    						AuthenticationException authException)
    										throws IOException, ServletException {
    	logger.error("Called Component: RestAuthenticationEntryPoint:commence");
        logger.error(PREFIX_ERROR_MESSAGE, authException.getMessage());

        Locale locale = LocaleContextHolder.getLocale();
        String errorMessage = messageSource.getMessage("exception.authentication_required", null, locale);

        EsResponse msg = new EsResponse(HttpStatus.UNAUTHORIZED, HttpStatus.UNAUTHORIZED.value(), errorMessage);

        response.reset();
        response.setCharacterEncoding("UTF-8");
    	response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    	response.setContentType("application/json");
    	logger.error("Operation is not authorized");

        PrintWriter out = response.getWriter();
    	out.print(msg.toJson());
    }

}
