package fr.testamento.es.security;

import static fr.testamento.es.URLConstants.APP_VERSION_URL;
import static fr.testamento.es.URLConstants.HEALTH_ENDPOINT_URL;
import static fr.testamento.es.utils.Messages.ACCESS_DENIED;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
@Order(1)
public class APISecurityConfig extends WebSecurityConfigurerAdapter {

	@Value("${http.auth-header-name}")
    private String principalRequestHeader;

	@Value("${http.auth-header-value}")
    private String principalRequestValue;

	@Autowired
    private RestAuthenticationEntryPoint authenticationEntryPoint;

	@Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        APIKeyAuthFilter filter = new APIKeyAuthFilter(principalRequestHeader);
        filter.setAuthenticationManager(authenticationManager());

        httpSecurity
        			.addFilter(filter)
        			.authorizeRequests()
        				.antMatchers(HttpMethod.GET, HEALTH_ENDPOINT_URL).permitAll()
        				.antMatchers(APP_VERSION_URL).permitAll()
        				.antMatchers("/resources/**").permitAll()
        				.anyRequest().authenticated()
        				.and()
        			.httpBasic()
        				.and()
        			.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint)
                    	.and()
                    .csrf().disable()
                    .cors().and()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

	@Override
	protected AuthenticationManager authenticationManager() {

		return authentication -> {
				String principal = (String) authentication.getPrincipal();
				if (!principalRequestValue.equals(principal)) {
					throw new BadCredentialsException(ACCESS_DENIED);
				}
				authentication.setAuthenticated(true);

				return authentication;
		};
	}

}
