package fr.testamento.es.service;

import static fr.testamento.es.utils.Messages.ERROR_RESPONSE_YOUSIGN;
import static fr.testamento.es.utils.Messages.INTERNAL_SERVER_ERROR;

import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import fr.testamento.es.exceptions.RestException;
import fr.testamento.es.utils.Utilities;
import fr.testamento.es.utils.YouSignErrorResponse;

@Service
public class ElectronicSignatureService {

	@Value("${yousign.api-key}")
	private String token;

	@Value("${yousign.base-url}")
	private String youSignUrl;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private Utilities utilities;

	private static final String YOUSIGN_MESSAGE = "There was an error connecting to YouSign: {}";
	private static final String REQUEST_MESSAGE = "The {} request: ({}) could not be completed";
	private static final String EXCEPTION_MESSAGE = "An error has occurred: {}";
	private static final Logger logger = LoggerFactory.getLogger(ElectronicSignatureService.class);

	public String generatePostRequest(Object request, String route) {
		logger.info("Called resource: generatePostRequest");

		// Add UTF-8 encoding
		restTemplate.getMessageConverters()
			.add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		headers.setBearerAuth(token);

		HttpEntity<Object> entity = new HttpEntity<>(request, headers);

		ResponseEntity<String> response;
		try {
			response = restTemplate.exchange(youSignUrl + route, HttpMethod.POST, entity, String.class);
		} catch (HttpClientErrorException | HttpServerErrorException exception){
			logger.error(REQUEST_MESSAGE, HttpMethod.POST.toString(), youSignUrl + route);
			logger.error(YOUSIGN_MESSAGE, exception.getResponseBodyAsString());
			YouSignErrorResponse errorResponse = utilities.stringToObject(exception.getResponseBodyAsString(), YouSignErrorResponse.class);

			throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, ERROR_RESPONSE_YOUSIGN,
 										new String[]{errorResponse.getDetail()});
		} catch(Exception exception){
			logger.error(REQUEST_MESSAGE, HttpMethod.POST.toString(), youSignUrl + route);
			logger.error(EXCEPTION_MESSAGE, exception.getMessage());

			throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR);
		}

		return response.getBody();
	}

	public String generatePutRequest(Object request, String route) {
		logger.info("Called resource: generatePutRequest");

		// Add UTF-8 encoding
		restTemplate.getMessageConverters()
			.add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		headers.setBearerAuth(token);

		HttpEntity<Object> entity = new HttpEntity<>(request, headers);

		ResponseEntity<String> response;
		try {
			response = restTemplate.exchange(youSignUrl + route, HttpMethod.PUT, entity, String.class);
		}catch (HttpClientErrorException | HttpServerErrorException exception){
			logger.error(REQUEST_MESSAGE, HttpMethod.PUT.toString(), youSignUrl + route);
			logger.error(YOUSIGN_MESSAGE, exception.getResponseBodyAsString());
			YouSignErrorResponse errorResponse = utilities.stringToObject(exception.getResponseBodyAsString(), YouSignErrorResponse.class);

			throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, ERROR_RESPONSE_YOUSIGN,
 										new String[]{errorResponse.getDetail()});

		}catch(Exception exception){
			logger.error(REQUEST_MESSAGE, HttpMethod.PUT.toString(), youSignUrl + route);
			logger.error(EXCEPTION_MESSAGE, exception.getCause());

			throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR);
		}

		return response.getBody();
	}

	public byte[] generateGetRequest(String route) {
		logger.info("Called resource: generateGetRequest");

		// Add UTF-8 encoding
		restTemplate.getMessageConverters()
			.add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		headers.setBearerAuth(token);

		HttpEntity<Object> request = new HttpEntity<>(null, headers);

		ResponseEntity<byte[]> response;
		try {
			response = restTemplate.exchange(youSignUrl + route, HttpMethod.GET, request, byte[].class);
		}catch (HttpClientErrorException | HttpServerErrorException exception){
			logger.error(REQUEST_MESSAGE, HttpMethod.GET.toString(), youSignUrl + route);
			logger.error(YOUSIGN_MESSAGE, exception.getResponseBodyAsString());
			YouSignErrorResponse errorResponse = utilities.stringToObject(exception.getResponseBodyAsString(), YouSignErrorResponse.class);

			throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, ERROR_RESPONSE_YOUSIGN,
 										new String[]{errorResponse.getDetail()});
		}catch(Exception exception){
			logger.error(REQUEST_MESSAGE, HttpMethod.GET.toString(), youSignUrl + route);
			logger.error(EXCEPTION_MESSAGE, exception.getCause());

			throw new RestException(HttpStatus.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR);
		}

		return response.getBody();
	}


}
