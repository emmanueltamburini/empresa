package fr.testamento.es;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import fr.testamento.es.exceptions.RestException;
import fr.testamento.es.utils.EsResponse;

@ControllerAdvice
public class ErrorExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
    private MessageSource messageSource;

	private static final String PREFIX_ERROR_MESSAGE = "Exception message: ";

	@ExceptionHandler({RestException.class})
	public ResponseEntity<Object> handleCustomExceptions(RestException ex) {
        logger.error("Called Handler: ErrorExceptionHandler:handleExceptions");

        Locale locale = LocaleContextHolder.getLocale();
        String[] args = ex.getArgs();

        List<String> argLocalized = new ArrayList<>();
        for(String textToTranslate : args){
            argLocalized.add(messageSource.getMessage(textToTranslate, null, locale));
        }

        String errorMessage = messageSource.getMessage(ex.getMessage(), argLocalized.toArray(), locale);
        logger.error(PREFIX_ERROR_MESSAGE + errorMessage);

        EsResponse response = new EsResponse(ex.getStatus(), ex.getStatus().value(), errorMessage);

        return new ResponseEntity<>(response, ex.getStatus());
    }

	@Override
	public ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
																	  HttpHeaders headers,
																	  HttpStatus status,
																	  WebRequest request) {
		logger.error("Called Handler: ErrorExceptionHandler:handleHttpRequestMethodNotSupported");

		Locale locale = LocaleContextHolder.getLocale();
		Object[] args = new Object[] { ex.getMethod()};

		String errorMessage = messageSource.getMessage("exception.method_not_supported", args, locale);
		logger.error(PREFIX_ERROR_MESSAGE + errorMessage);
		EsResponse response = new EsResponse(status, status.value(), errorMessage);

		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	@Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex,
                                                                   HttpHeaders headers,
                                                                   HttpStatus status,
                                                                   WebRequest request) {
		logger.error("Called Handler: ErrorExceptionHandler:handleNoHandlerFoundException");

		Locale locale = LocaleContextHolder.getLocale();
        Object[] args = new Object[] { ex.getHttpMethod(), ex.getRequestURL() };

        String errorMessage = messageSource.getMessage("exception.not_found", args, locale);
        logger.error(PREFIX_ERROR_MESSAGE + ex.getMessage());
        EsResponse response = new EsResponse(status, status.value(), errorMessage);

        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);

    }

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
		logger.error("Called Handler: ErrorExceptionHandler:handleAll");

        Locale locale = LocaleContextHolder.getLocale();
        String errorMessage = messageSource.getMessage("exception.internal_server_error", null, locale);
        logger.error(PREFIX_ERROR_MESSAGE + ex.getMessage());

        EsResponse response = new EsResponse(HttpStatus.INTERNAL_SERVER_ERROR,
        										HttpStatus.INTERNAL_SERVER_ERROR.value(), errorMessage);

        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
                                                                          HttpHeaders headers,
                                                                          HttpStatus status,
                                                                          WebRequest request)
    {
        Locale locale = LocaleContextHolder.getLocale();
        Object[] args = new Object[] { ex.getParameterType(), ex.getParameterName() };
        String errorMessage = messageSource.getMessage("exception.missing_parameter", args, locale);

        logger.error(PREFIX_ERROR_MESSAGE + ex.getMessage());

        EsResponse response = new EsResponse(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.value(), errorMessage);

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
