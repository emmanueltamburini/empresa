## Development area

### Requirements:
* OpenJDK 11 or greater
* Maven 3.3.9 or greater
* Docker version 18.03.1-ce

### How to run it during development:
* Using maven (Backend API spring live running): `mvn spring-boot:run`

### How to build and package this:
* Using maven: `mvn clean package`

### How to test
* Using maven: `mvn test`

### Some Dev references:

* [Spring Data REST](https://docs.spring.io/spring-data/rest/docs/current/reference/html/)
* [Structuring Your Code](https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-structuring-your-code.html)

#### Establishing Docker repository URL

By default the **artifactory.docker.server** property in the **pom.xml** is empty, before proceeding to package or build the docker image you need to define this property in the **pom.xml** or in the maven profile (**settings.xml**)

**pom.xml**

Edit the **pom.xml** and point in the **artifactory.docker.server** the corresponding repository host

```
<artifactory.docker.server>{url}</artifactory.docker.server>
```

Where the **{url}** is the value to set

Example:
```
<artifactory.docker.server>012345678910.dkr.ecr.us-east-1.amazonaws.com</artifactory.docker.server>
```

**settings.xml**

Following the link [Introduction to profiles](https://maven.apache.org/guides/introduction/introduction-to-profiles.html), you can configure the **artifactory.docker.server** as a global property defined in the maven profiles **settings.xml**

To do that you need to create or edit the file **(%USER_HOME%/.m2/settings.xml)**, add or edit an active profile, and put the **artifactory.docker.server** in the **\<properties>** tag. You can use an **activeByDefault** profile or use the profile in the command line as an argument.

```
...
<profiles>
	<profile>
		<activation>
			<activeByDefault>true</activeByDefault>
		</activation>
		<properties>
			<artifactory.docker.server>{url}</artifactory.docker.server>
		</properties>
	</profile>
</profiles>
...
```

Where the **{url}** is the value to set

Example:
```
...
<profiles>
	<profile>
		<activation>
			<activeByDefault>true</activeByDefault>
		</activation>
		<properties>
			<artifactory.docker.server>012345678910.dkr.ecr.us-east-1.amazonaws.com</artifactory.docker.server>
		</properties>
	</profile>
</profiles>
...
```

**Note**: before configuring the repository, you must log in to the Docker repository to have access to push the Docker image (if is required). For AWS ECR repository follow the link [Amazon ECR Registries](https://docs.aws.amazon.com/AmazonECR/latest/userguide/registries.html#registry_auth) and for another repository [Docker Login](https://docs.docker.com/engine/reference/commandline/login/)

#### Establishing application properties

Before running the application, it is necessary to set the corresponding properties in the application.properties file, which are:

``` 
...
yousign.base-url={url}
yousign.api-key={string}
yousign.validation-type={string}
yousign.validation-message={string}

...
signature.position={string}
signature.page-position={integer}
signature.top-mention={string}
signature.bottom-mention={string}
signature.image-path={path}

...
date.format=dd/MM/yyyy
datetime.format=dd/MM/yyyy HH:mm:ss

...
http.auth-header-name={string}
http.auth-header-value={string}

```

The values:

* **yousign.base-url** is the url of the Yousign Api Service

* **yousign.api-key** is the secret API key that allows access to Yousign

* **yousign.validation-type** is the type of verification used to validate a signer, where the security code will be sent, also named OTP (One Time Password). Valid values ​​for this property is (**sms** or **email**)

* **yousign.validation-message** It is the custom message used to validate a signer. This message requires to include the **{{code}}** field to refer to the OTP code. Additional you can use other customizable parameters in the message such as:

**{{firstName}}**
**{{lastName}}**
**{{email}}**
**{{phoneNumber}}**
**{{date}}**
**{{datetime}}**

**Note:** By default if this property is empty, the default validation message of yousign will be used

* **signature.position** define the coordinates of the digital signature that will be affixed to your PDF document

To do this, you can define the coordinates using the following format: "llx,lly,urx,urx,ury".

**llx** = X-axis (definition of horizontal size). This is the space from the left side of the page to the left side of the visual signature ("left lower x").

**lly** = Y-axis (height definition). This is the space from the bottom of the page to the bottom of the visual signature ("left lower y").

**urx** = X-axis (definition of horizontal size). This is the space from the left side of the page to the right side of the visual signature ("upper right x").

**ury** = Y-axis (height definition). This is the space from the bottom of the page to the top of the visual signature ("upper right y").

**Example:** "400,700,500,800".

**Note:** In order to define these coordinates, you can use the Placeit tool: **https://placeit.yousign.fr/**

* **signature.page-position** define the page number within the document where the signature will be inserted

* **signature.top-mention** optional field that add information on the signature image. This property is displayed on top of signature image. Additional you can use other customizable parameters in the message such as:

**{{firstName}}**
**{{lastName}}**
**{{email}}**
**{{phoneNumber}}**
**{{date}}**
**{{datetime}}**

* **signature.bottom-mention** optional field that add information on the signature image.  This property  is displayed on bottom of signature image. Additional you can use other customizable parameters in the message such as:

**{{firstName}}**
**{{lastName}}**
**{{email}}**
**{{phoneNumber}}**
**{{date}}**
**{{datetime}}**

* **signature.image-path** is the path of the signature image will be used to sign the documents.

**Note:** By default if this property is empty, the default signature image of yousign will be used

* **date.format** is the format of the parameter **{{date}}** that will be replaced in the sms or signature mentions. By default is: dd/MM/yyyy

* **datetime.format** is the format of the parameter **{{datetime}}** that will be replaced in the sms or signature mentions. By default is: dd/MM/yyyy HH:mm:ss

* **http.auth-header-name** is the custom header used by the application to authorize the request.

* **http.auth-header-value** is the credential used by the application to authorize the request.

#### About endpoints

Each endpoint contains helpful information about how to use it, in a rest-client or using a third-party app. This information is composed of:

* **URL**: represents the endpoint URL.
* **Method**: represents the request **Method** accepted by the endpoint.
* **Authentication**: represents if the request needs authentication or not.
* **Content Type**: represents the **Content-Type** accepted by the endpoint (if it is required).
* **Body**: represents the body information (if it is required).


**NOTE**: To enable authorization, a custom header must be added in the request with the values ​​assigned in the properties

* **http.auth-header-name** 
* **http.auth-header-value**

#### About Signature Status

The status field for a signer can have many values:

* **PENDING**: The signer has been invited to sign but has not yet signed..
* **PROCESSING**: The signatory has signed and our system has received this request and is processing it (it's a temporary status).
* **DONE**: The signer has signed and our system has carried out the signature on all documents. If the documents are downloaded from this moment on, they will contain the signature of the signer concerned.
* **REFUSED**: The signer refused to sign or validate the procedure


## Signature endpoints:

### Generate signature

To start signing a document you can use the **Generate signature** -> **Endpoint**.

#### Endpoint

* **URL**: **https://<base_url>/es/api/signature/generate** 
* **Method**: POST
* **Authentication**: required
* **Content Type**: application/x-www-form-urlencoded
* **Content Type**: application/x-www-form-urlencoded
* **Response Type**: application/json 
* **Body**: the body data use the following Form:

```
KEY  				|  VALUE
firstName			|  String
lastName			|  String
phoneNumber			|  String
email				|  String
file				|  byte[]
```

All fields are required. This endpoint triggers the sending of the otp code to validate the signer.

The response of this endpoint is:

```
{
    "file": "",
    "signatory": "",
    "authentication": "",
    "status": ""
}
```

The values:

**file** represents the id of the document that will be used to retrieve it after signing it

**signatory** is the signer id

**authentication** It is the reference of the otp code sent to the signer

**status** It is the signature status 


### Update signature

If you do not receive the otp code, you must update the previously created signature process to receive a new code, you can use the **Update signature** -> **Endpoint**.

#### Endpoint

* **URL**: **https://<base_url>/es/api/signature/generate** 
* **Method**: PUT
* **Authentication**: required
* **Content Type**: application/x-www-form-urlencoded
* **Response Type**: application/json 
* **Body**: the body data use the following Form:

```
KEY  				|  VALUE
signatory			|  String
```

This endpoint triggers again the sending of the otp code, returning a new reference for the code

The response of this endpoint is:

```
{
    "authentication": "",
    "status": ""
}
```

The values:

**authentication** It is the reference of the otp code sent to the signer
**status** It is the signature status 

### Validate signature

After receiving the code, the signer must be validated to proceed with the signing of the document, you can use the **Validate signature** -> **Endpoint**.

#### Endpoint

* **URL**: **https://<base_url>/es/api/signature/validate** 
* **Method**: POST
* **Authentication**: required
* **Content Type**: application/x-www-form-urlencoded
* **Response Type**: application/json 
* **Body**: the body data use the following Form:

```
KEY  				|  VALUE
authentication		|  String
phoneCode			|  String
```

Where the **authentication** field is the response of the request created before and **phoneCode** is the received otp code.

When the signer is validated, the status of the signature will be returned.

The response of this endpoint is:

```
{
    "status": ""
}
```

The values:

**status** It is the signature status 

### Get Signature Status

After validate signature,  to retrieve the signature status you can use the **Get Signature Status** -> **Endpoint**.

#### Endpoint

* **URL**: **https://<base_url>/es/api/signature/status/<id>** (The **<id>** represents the id of the signer (**signatory**) obtained before)
* **Method**: GET
* **Authentication**: required
* **Response Type**: application/json 
* **Body**: the body data use the following Form:

The response of this endpoint is:

```
{
    "status": ""
}
```

The values:

**status** It is the signature status 

### Get Signature

Finally to retrieve the signature file you can use the **Get Signature** -> **Endpoint**.

#### Endpoint

* **URL**: **https://<base_url>/es/api/signature/pdf/<id>** (The **<id>** represents the id of the document (**file**) created before)
* **Method**: GET
* **Authentication**: required
* **Response Type**: application/pdf

This endpoint returns a **PDF** file.