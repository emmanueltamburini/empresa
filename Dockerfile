FROM openjdk:11-jre

ENTRYPOINT ["java", "-jar", "/opt/testamento-electronic-signature/es.jar", "--spring.config.location=classpath:/application.properties,file:/opt/testamento-electronic-signature/es/application.properties"]

ARG JAR_FILE
ADD target/${JAR_FILE} /opt/testamento-electronic-signature/es.jar

RUN mkdir -p /opt/testamento-electronic-signature/es/logs/
